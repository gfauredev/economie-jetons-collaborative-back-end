INSERT INTO `member` (`surname`,`name`,`address`,`zip_code`,`city`,`email`,`birth_date`,`password`,`admin`,`validity`)
VALUES
  ("Hewitt","Jaquelyn","Ap #523-6799 Urna. St.","38118","Ulyanovsk","coordinateur@ecojetons","1985-01-29","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("York","Athena","8670 Aliquam St.","50175","Vinh","member@ecojetons","1982-10-05","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","1","1"),
  ("Reilly","Lysandra","P.O. Box 425, 1777 Sed St.","22382","Kraków","lysandra.reilly@protonmail.com","1990-03-12","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","0"),
  ("Combs","Ila","8585 Gravida Rd.","33762","Gunsan","velit.aliquam.nisl@outlook.ca","1984-08-07","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Woodard","Kamal","2019 Fringilla Street","62842","Mora","fringilla.est.mauris@protonmail.org","1996-02-01","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1"),
  ("Guerra","Chancellor","209-1154 Dapibus St.","95930","Seletar","a.purus.duis@hotmail.com","1979-01-13","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Raymond","Audra","Ap #133-9109 Nulla St.","07174","Palmerston","pretium.neque.morbi@yahoo.couk","1997-07-09","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Bender","Mercedes","Ap #348-975 In Street","97388","Vienna","tincidunt@google.couk","1978-08-29","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Pope","Sylvia","Ap #860-8126 Neque Av.","81088","Jeonju","lorem@protonmail.couk","1990-12-05","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Knight","Martin","Ap #143-3195 Proin Road","68337","Rodez","tempor@aol.net","1980-11-09","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1");
INSERT INTO `member` (`surname`,`name`,`address`,`zip_code`,`city`,`email`,`birth_date`,`password`,`admin`,`validity`)
VALUES
  ("Stone","Kenneth","703-4249 Lobortis. Street","93120","Sicuani","diam.sed@hotmail.com","1976-08-31","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","1","1"),
  ("Morrow","Brenda","821 Ultrices, Av.","14938","Hà Nội","euismod.ac@icloud.org","1983-06-01","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Eaton","Nathan","Ap #995-4930 Lorem, Avenue","13935","Sicuani","velit.eu.sem@aol.couk","1977-07-03","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Browning","Amos","5299 Non, Rd.","19617","Los Mochis","mollis.vitae.posuere@icloud.ca","1973-03-05","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Ruiz","Nerea","6663 Odio Ave","12148","Soledad de Graciano Sánchez","justo.proin@icloud.edu","1990-06-11","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1"),
  ("Cantu","Cameron","921-9627 Aliquam Ave","03713","Slough","egestas.ligula.nullam@icloud.couk","1994-03-11","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Greene","Kennan","227-3267 Tellus Avenue","89173","Bordeaux","libero.lacus@outlook.com","1993-01-23","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Long","Astra","Ap #548-3496 Eu Rd.","09762","Kızılcahamam","nunc.sollicitudin@icloud.com","1982-04-24","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Lancaster","Troy","485-5923 Laoreet Ave","80165","Lustenau","et.ipsum.cursus@aol.org","1984-01-14","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Marks","Ezekiel","Ap #705-8651 Odio Avenue","72382","Seogwipo","vivamus.sit.amet@google.couk","1992-01-09","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","1","1");
INSERT INTO `member` (`surname`,`name`,`address`,`zip_code`,`city`,`email`,`birth_date`,`password`,`admin`,`validity`)
VALUES
  ("Turner","Ann","871-223 Maecenas Road","71193","Frederick","in@icloud.com","1985-07-29","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Blackburn","Grant","P.O. Box 699, 5422 Quam St.","90576","Cleveland","elementum.purus@yahoo.couk","1975-04-15","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Blankenship","Bertha","915-5638 Ut, Avenue","06699","Dos Hermanas","turpis.non@google.edu","1985-01-15","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Brooks","Winter","P.O. Box 793, 4547 Eget Avenue","60735","Fauske","metus.aliquam@aol.net","1988-08-21","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Garcia","Adena","Ap #159-7735 Varius Avenue","54265","Tarnów","ultrices.sit.amet@hotmail.edu","1995-02-06","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","0"),
  ("Morse","Jenette","951-6260 Nibh. Street","04652","Buenaventura","tristique@aol.net","1977-07-14","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Butler","Hayley","255-9334 Eu Street","33921","San José de Alajuela","natoque.penatibus@icloud.ca","1981-09-15","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("English","Zane","602-368 Vulputate Road","39832","Stafford","nisi.sem.semper@icloud.ca","1988-12-24","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Porter","Erica","Ap #662-1112 Lectus Rd.","62921","Penrith","vel.faucibus@icloud.net","1996-05-02","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Hall","Jennifer","Ap #928-7025 Mattis St.","88156","Rio Verde","magna.praesent@yahoo.org","1999-02-15","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1");
INSERT INTO `member` (`surname`,`name`,`address`,`zip_code`,`city`,`email`,`birth_date`,`password`,`admin`,`validity`)
VALUES
  ("Dyer","Allegra","P.O. Box 147, 4112 Imperdiet Rd.","91686","Swabi","fusce.fermentum@hotmail.net","1990-07-27","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Roth","Logan","5371 Ullamcorper. Av.","51873","Galway","euismod@outlook.org","1977-06-27","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","0"),
  ("Luna","Katell","438-297 Molestie Av.","38422","Enschede","nibh.sit@yahoo.ca","1996-03-03","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Copeland","Seth","4169 Molestie Rd.","65531","Vinh","libero.morbi@google.org","1974-01-16","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Morrow","Amery","741-2790 Elementum, St.","06793","Neustrelitz","integer.urna.vivamus@outlook.edu","1974-10-16","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1"),
  ("Davenport","Levi","807-9037 Id Road","31704","Lerum","semper.dui@outlook.ca","1972-05-12","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","1","0"),
  ("Sanford","Keiko","Ap #125-3894 Tellus Ave","98505","King William's Town","dolor@protonmail.org","1991-12-12","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Beck","Anastasia","234-6574 Elit, Street","16476","Oaxaca","eros.turpis@protonmail.net","1979-12-22","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Burke","Nell","750-3811 Et, St.","31274","Okene","lorem.donec@yahoo.ca","1994-03-30","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Ayers","Travis","P.O. Box 942, 3876 A, Road","46966","Cần Thơ","metus@outlook.couk","1990-06-13","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","0");
INSERT INTO `member` (`surname`,`name`,`address`,`zip_code`,`city`,`email`,`birth_date`,`password`,`admin`,`validity`)
VALUES
  ("Spears","Iliana","512-9880 Arcu St.","88847","Onitsha","pretium.et@protonmail.net","1974-12-12","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Pickett","Winter","585-1536 Vitae, Avenue","56457","Wattrelos","vel.arcu.eu@google.edu","1985-11-15","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Montgomery","Jessica","Ap #784-8875 Ipsum. Ave","17231","Rugby","eleifend.cras@aol.net","1983-01-23","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Dickerson","Jaime","Ap #637-6317 Consectetuer Ave","78720","Killa Abdullah","vitae.erat@outlook.net","1971-02-23","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Snyder","Caleb","190-5491 Suspendisse St.","48698","Dipignano","egestas.duis@protonmail.couk","1985-05-05","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1"),
  ("Fisher","Bell","P.O. Box 274, 9710 Orci St.","79707","Velden am Wörther See","proin.sed.turpis@google.com","1999-02-02","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Becker","Zeus","Ap #488-4012 At Rd.","44036","La Serena","orci.lacus@yahoo.edu","1983-04-12","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Blackburn","Graiden","P.O. Box 342, 7239 Ornare St.","92172","Tierra Amarilla","sed.neque@google.edu","1996-10-03","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Nichols","Nathaniel","123 Enim St.","00689","Lebowakgomo","massa.mauris@aol.couk","1972-02-11","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Cain","Hannah","Ap #471-5975 Enim Road","18036","Heerenveen","fermentum.arcu.vestibulum@protonmail.org","1996-04-27","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1");
INSERT INTO `member` (`surname`,`name`,`address`,`zip_code`,`city`,`email`,`birth_date`,`password`,`admin`,`validity`)
VALUES
  ("Bass","Elmo","Ap #892-957 Per St.","12644","Bandar Lampung","placerat.orci@yahoo.edu","1977-08-16","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Morrow","Allen","Ap #437-3876 Pharetra. St.","35065","Charlecity-Mézières","purus.accumsan.interdum@icloud.edu","1980-02-08","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Romero","Gabriel","461-3172 Sed, Rd.","28572","Kolhapur","ipsum@protonmail.edu","1995-05-14","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Duncan","Vladimir","Ap #678-3785 Dapibus Rd.","25964","Baunatal","lobortis.quam@yahoo.ca","1995-05-20","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Bird","Kelsey","847-9879 Molestie Avenue","39611","Jeju","sed.nec@yahoo.couk","1977-11-18","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","1","1"),
  ("Weber","Walker","P.O. Box 147, 2056 Enim Avenue","37819","Falun","luctus@icloud.org","1988-10-11","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Cummings","Lewis","3744 Adipiscing St.","34486","Pointe-aux-Trembles","enim.consequat@yahoo.ca","1993-10-17","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","0"),
  ("Short","Barbara","4431 Amet Ave","40386","Quy Nhơn","ut.eros@hotmail.com","1993-04-13","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Sutton","Victor","733-8210 Sem Road","86245","Chełm","eleifend.non@yahoo.com","1981-06-21","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","1","1"),
  ("Cotton","Eliana","Ap #488-8317 In Avenue","84428","Cheyenne","elit.dictum@google.ca","1979-03-30","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1");
INSERT INTO `member` (`surname`,`name`,`address`,`zip_code`,`city`,`email`,`birth_date`,`password`,`admin`,`validity`)
VALUES
  ("Adkins","Xander","2966 Tristique Street","04835","Windermere","ultrices.duis@icloud.ca","1982-07-30","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","1","1"),
  ("Shaw","Rashad","Ap #837-4550 Torquent Rd.","98655","Covington","laoreet.posuere.enim@yahoo.net","1979-08-29","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Cote","Kenyon","Ap #887-1380 Mi. Road","72536","Palu","morbi@aol.org","1978-04-01","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Tate","Raphael","4820 Lobortis St.","17731","Vallentuna","quam.pellentesque.habitant@yahoo.com","1996-06-18","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Emerson","Kato","489-7242 Enim. Avenue","21754","Padre Hurtado","aliquam.enim@yahoo.ca","1994-11-19","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1"),
  ("Parks","Ignatius","621-3971 Tincidunt Av.","92675","Yeosu","enim.non@aol.ca","1981-05-04","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Fernandez","Wynne","P.O. Box 846, 6902 Nullam Av.","82726","Gdynia","justo.sit@google.net","1981-04-20","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Head","Omar","650-6899 At, Rd.","17923","Zaria","rutrum.eu@protonmail.net","1971-03-12","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","0"),
  ("Kirkland","Quamar","484-5156 Erat Street","34735","Minderhout","convallis.est@icloud.net","1992-10-16","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Riley","Colby","319-3805 Adipiscing Road","36111","Bengkulu","amet@aol.net","1994-09-17","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1");
INSERT INTO `member` (`surname`,`name`,`address`,`zip_code`,`city`,`email`,`birth_date`,`password`,`admin`,`validity`)
VALUES
  ("Arnold","Lillith","579-7201 Tincidunt, Street","61735","Bandırma","rhoncus@aol.edu","1995-02-09","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Newman","Danielle","Ap #619-4054 At, Avenue","58761","Vetlanda","dui.fusce@hotmail.couk","1994-11-22","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","0"),
  ("Puckett","Aladdin","Ap #139-2619 Ac Ave","49366","Nha Trang","morbi.metus@protonmail.edu","1979-06-26","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Barber","Hayley","Ap #638-7405 Sapien, Ave","72703","Chandigarh","arcu.ac@yahoo.ca","1989-05-03","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Dyer","Inez","Ap #922-7149 Luctus Rd.","63272","Hamilton","velit.justo.nec@hotmail.com","1982-09-03","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1"),
  ("Soto","Laurel","214-644 Pede Street","56142","Samara","parturient@yahoo.couk","1989-05-30","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Woods","Fritz","P.O. Box 998, 7353 Arcu Road","49912","Muzaffargarh","venenatis.vel.faucibus@icloud.couk","1970-07-09","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Keith","Zahir","307-239 Interdum Road","55198","Lozova","convallis.ligula@outlook.net","1987-12-22","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Blanchard","Ocean","1438 Elit Rd.","56086","Limón (Puerto Limón]","ut@yahoo.com","1972-10-13","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Rhodes","Renee","100-7220 Nec, Ave","62812","Zwolle","luctus.felis@protonmail.org","1989-07-21","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1");
INSERT INTO `member` (`surname`,`name`,`address`,`zip_code`,`city`,`email`,`birth_date`,`password`,`admin`,`validity`)
VALUES
  ("Conway","Donna","449-126 Massa. St.","49987","Diepenbeek","ante@outlook.org","1981-02-16","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Larsen","Morgan","P.O. Box 181, 2730 Nonummy Avenue","73056","Ahmedabad","mauris@hotmail.com","1972-02-19","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Carpenter","Jakeem","308-1559 Nibh St.","90739","Kansas City","ipsum.cursus.vestibulum@outlook.com","1973-09-01","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Marks","Keelie","Ap #727-194 Montes, Road","09678","Caloundra","at.arcu@outlook.edu","1994-04-12","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Peck","Blythe","Ap #641-2771 Dolor Rd.","22483","Albacete","sapien.molestie@google.ca","1998-09-28","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1"),
  ("Velazquez","Alec","P.O. Box 989, 1124 Non, Street","53678","Quy Nhơn","adipiscing.fringilla@hotmail.org","1998-09-07","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Rojas","Stewart","1842 Mi. Street","03876","Halesowen","convallis.erat.eget@outlook.com","1998-12-19","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Best","Chancellor","P.O. Box 813, 6408 Convallis St.","24259","Lima","consequat@icloud.net","1992-07-25","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Patton","Austin","412-9584 Eu Street","03344","Nonsan","massa.integer@yahoo.com","1974-01-14","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","1","0"),
  ("Guthrie","John","P.O. Box 266, 2093 Vestibulum St.","82640","León","metus.urna@protonmail.ca","1976-06-22","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1");
INSERT INTO `member` (`surname`,`name`,`address`,`zip_code`,`city`,`email`,`birth_date`,`password`,`admin`,`validity`)
VALUES
  ("Galloway","Yoshio","6652 Pede, St.","19782","Itanagar","pede.praesent.eu@aol.net","1991-07-28","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Moran","Barrett","6066 Phasellus St.","30867","Nemi","risus.odio@icloud.org","1976-08-23","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Ortiz","Flavia","Ap #180-9334 Fusce Av.","84566","Cape Town","eget.dictum.placerat@hotmail.com","1982-03-05","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Norris","Jackson","558-3773 Ante Ave","78118","Petrolina","sed.eu.eros@outlook.ca","1980-06-15","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("West","Jack","600-8712 Cras St.","81214","Andernach","euismod.et@protonmail.net","1981-11-11","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1"),
  ("Houston","Sierra","348-2592 Aliquam Avenue","05189","Hamilton","risus.nunc@google.net","1977-09-10","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Wynn","Harrison","764-5736 Nibh St.","64668","Greater Hobart","arcu.aliquam.ultrices@google.net","1976-05-27","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Berg","Naida","233-4182 Nibh. St.","54323","Kavaratti","lorem@aol.edu","1988-07-20","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("O'Neill","Fay","1125 Porttitor St.","07201","Central Water Catchment","tellus.phasellus.elit@outlook.edu","1986-12-30","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Larsen","Hop","P.O. Box 456, 6970 Mauris Av.","31841","Dandenong","aliquam.vulputate@protonmail.com","1989-06-09","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1");
INSERT INTO `member` (`surname`,`name`,`address`,`zip_code`,`city`,`email`,`birth_date`,`password`,`admin`,`validity`)
VALUES
  ("Hodge","Steel","2701 Nec Street","25862","Ramsey","felis.orci@hotmail.couk","1991-03-12","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","1","1"),
  ("Colon","Malcolm","Ap #606-4067 Cursus, Road","22415","Sandviken","nunc.est.mollis@protonmail.net","1992-05-05","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","1","1"),
  ("May","Lewis","Ap #521-3029 Pede Street","15817","Tarakan","nonummy.ac@hotmail.couk","1993-08-04","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","0"),
  ("Abbott","Kirk","361-3642 Integer Avenue","78532","Chernihiv","morbi.quis@protonmail.edu","1973-09-09","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Torres","Bert","P.O. Box 355, 3099 Pellentesque St.","23956","Butterworth","mauris.ut.mi@icloud.edu","1978-05-23","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1"),
  ("Pollard","Beau","Ap #445-7571 Integer Road","84545","Boechout","nibh.dolor.nonummy@hotmail.com","1986-08-18","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Blackburn","Michelle","P.O. Box 777, 454 Quis St.","94635","Picton","ante.blandit@hotmail.org","1998-05-07","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Wise","Dean","370-1116 Leo. Avenue","74342","Limoges","fermentum.fermentum@hotmail.org","1980-01-21","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Gilbert","Berk","P.O. Box 388, 9349 Eu St.","61812","Rotterdam","nulla.dignissim.maecenas@icloud.edu","1986-12-12","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Owen","Julie","P.O. Box 306, 3967 Vulputate, Street","44855","North-Eastern Islands","aliquet.odio@icloud.edu","1998-06-18","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1");
INSERT INTO `member` (`surname`,`name`,`address`,`zip_code`,`city`,`email`,`birth_date`,`password`,`admin`,`validity`)
VALUES
  ("Rowland","Kiona","Ap #385-655 Fringilla. Ave","42555","Traun","laoreet.libero@hotmail.org","1986-09-20","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","1","0"),
  ("Cortez","Pearl","247-4883 Cursus. Rd.","27326","Machalí","aliquam@google.edu","1986-07-11","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Poole","India","P.O. Box 437, 9998 Et, Road","43761","Rochor","nec.luctus@yahoo.net","1977-09-08","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("May","Patrick","Ap #558-4985 Nunc Ave","84981","Panjim","vitae.odio@yahoo.com","1976-04-21","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Mcintyre","Rhiannon","P.O. Box 860, 4416 Mollis. Av.","55628","Chepén","duis@yahoo.org","1977-12-13","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1"),
  ("Mills","Ruby","8122 Donec Street","68243","Ruddervoorde","aliquet.odio.etiam@outlook.edu","1975-01-01","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Aguirre","Fuller","637-7588 Ullamcorper Rd.","03134","Celaya","quis.turpis@google.couk","1995-05-08","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Koch","Rooney","7361 Nunc, Av.","96709","Minneapolis","nisi@aol.net","1977-12-14","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Andrews","Oprah","Ap #144-7514 Lacus. Street","56371","Pematangsiantar","arcu@protonmail.ca","1971-04-13","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","0"),
  ("Lara","Griffith","P.O. Box 315, 4181 Tempor St.","75928","Bắc Giang","magna.suspendisse.tristique@hotmail.com","1993-05-18","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1");
INSERT INTO `member` (`surname`,`name`,`address`,`zip_code`,`city`,`email`,`birth_date`,`password`,`admin`,`validity`)
VALUES
  ("Davidson","Harlan","7897 Lobortis. St.","79454","Falun","dis@google.couk","1976-06-07","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Ferguson","Richard","Ap #214-9766 Sapien, Rd.","11822","Pangkalpinang","mauris@yahoo.org","1982-10-26","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Hurley","Harlan","P.O. Box 891, 1212 Blandit Rd.","77164","Kroonstad","elit.pharetra@yahoo.ca","1973-02-02","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Rocha","Kylie","4416 Consequat Rd.","51550","Tver","vivamus@icloud.edu","1991-10-12","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Holcomb","Burke","978-9397 Mollis Rd.","83686","Sechura","metus.aenean@aol.edu","1974-11-16","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","0"),
  ("Gross","Gregory","Ap #706-199 Augue. St.","12667","Ulloa (Barrial]","magna.duis@aol.org","1971-09-05","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Delaney","Rigel","Ap #227-8284 Metus. Ave","62178","Jaén","libero.mauris.aliquam@icloud.org","1982-01-28","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Oneil","Adrian","391-3956 Quis, Rd.","51765","Montpelink","fringilla@google.couk","1972-02-19","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("English","Julian","Ap #942-4383 Id, St.","62517","Butuan","malesuada.id.erat@protonmail.ca","1984-04-05","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Stuart","Jerry","486-6308 Fusce Avenue","97396","Fort Laird","neque@google.org","1998-05-03","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1");
INSERT INTO `member` (`surname`,`name`,`address`,`zip_code`,`city`,`email`,`birth_date`,`password`,`admin`,`validity`)
VALUES
  ("Michael","Cruz","P.O. Box 979, 8568 Mauris Rd.","70548","Berdiansk","dictum.sapien@aol.edu","1994-09-23","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","0"),
  ("Schneider","Lawrence","5118 Metus St.","73140","Winnipeg","mattis@protonmail.ca","1993-11-13","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Jacobson","Flynn","Ap #844-5469 Proin St.","22599","Fort Smith","parturient.montes.nascetur@yahoo.net","1979-04-12","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Conway","Shaeleigh","6158 Leo. Ave","29255","Kufstein","amet.orci@protonmail.ca","1982-07-25","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Gamble","Lesley","P.O. Box 622, 1910 Malesuada Street","71862","Gingoog","dictum.phasellus.in@protonmail.couk","1988-03-25","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1"),
  ("Norman","Noble","P.O. Box 786, 4491 Egestas St.","54131","Quy Nhơn","velit.eu@aol.com","1987-01-07","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Thomas","Silas","Ap #834-2750 Enim, Street","85671","Hudiksvall","mattis.ornare.lectus@hotmail.com","1980-07-19","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Reed","Hope","P.O. Box 662, 9031 Metus Av.","85321","Ciudad Madero","neque.in@outlook.net","1991-10-06","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Stevenson","Griffin","Ap #467-9189 Arcu. Street","35516","Awka","netus.et@outlook.couk","1980-05-06","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Kane","Kessie","616-8415 Non, Road","59453","Vashkivtsi","quis.urna.nunc@icloud.edu","1970-04-10","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1");
INSERT INTO `member` (`surname`,`name`,`address`,`zip_code`,`city`,`email`,`birth_date`,`password`,`admin`,`validity`)
VALUES
  ("Shaffer","Noble","Ap #812-8568 Cras Street","63591","Toulouse","sed.molestie@hotmail.couk","1991-04-19","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Terry","Ishmael","8003 Dignissim. St.","14204","Deventer","aliquam@protonmail.net","1975-03-09","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Norton","Macaulay","Ap #769-8038 Vestibulum Road","55804","Vänersborg","ornare.sagittis.felis@yahoo.net","1984-02-27","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Perry","Lewis","Ap #842-6602 Natoque Rd.","81283","Jecheon","at.velit@google.org","1981-09-08","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Mcknight","Marny","Ap #535-7614 Augue Street","53593","Tourinnes-la-Grosse","aenean@yahoo.edu","1975-06-29","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1"),
  ("Burke","Ignacia","P.O. Box 588, 7322 Pharetra St.","88843","Tam Kỳ","risus.nunc.ac@google.couk","1982-03-28","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Marks","Aiko","214 Eu Rd.","27583","Smithers","integer.vulputate.risus@aol.couk","1990-10-09","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","1","1"),
  ("Meyers","Autumn","163-4065 Augue Street","86428","Clementi","dui@protonmail.com","1972-07-27","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Mcneil","Odysseus","837-3545 Feugiat Road","06110","Sale","nulla.donec@icloud.ca","1974-06-25","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Melton","Kelsie","984-8934 A Rd.","92482","Cáceres","diam.luctus.lobortis@yahoo.net","1982-05-07","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1");
INSERT INTO `member` (`surname`,`name`,`address`,`zip_code`,`city`,`email`,`birth_date`,`password`,`admin`,`validity`)
VALUES
  ("Michael","Merritt","Ap #610-6601 Eget, Av.","38646","Casacalenda","pellentesque.habitant@icloud.ca","1971-10-23","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Trevino","Amaya","632-8210 Ut Road","36911","Hamburg","nunc.laoreet@hotmail.couk","1970-06-11","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Bird","Leslie","596-4280 Feugiat Rd.","82358","Kovel","mauris.sit@hotmail.net","1976-08-19","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Scott","Jenna","P.O. Box 845, 7795 Ultrices. Street","65624","North Las Vegas","massa.quisque.porttitor@outlook.ca","1981-08-11","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("King","Ashely","P.O. Box 346, 9033 Dis Av.","79495","Flushing","odio.a.purus@google.org","1992-10-12","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1"),
  ("Sharp","Tanner","575-3706 Cras Road","56173","Lüneburg","lacus.nulla@aol.net","1990-05-13","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Decker","Gray","Ap #734-8979 Ipsum St.","65240","Queenstown","donec@aol.org","1975-03-03","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","0"),
  ("Mcintosh","Eve","Ap #356-3701 Imperdiet Street","91472","Seoul","enim@aol.net","1994-03-22","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Valentine","Jamalia","747-4089 Donec St.","50692","Oyo","dignissim.magna@protonmail.net","1985-11-16","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","0"),
  ("Cote","Jordan","698-8497 Nec, Ave","02157","Puntarenas","aliquet@hotmail.edu","1996-05-31","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1");
INSERT INTO `member` (`surname`,`name`,`address`,`zip_code`,`city`,`email`,`birth_date`,`password`,`admin`,`validity`)
VALUES
  ("Quinn","Risa","560-8894 Tincidunt Ave","58874","Valparaíso de Goiás","eget.nisi.dictum@google.net","1982-10-26","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Dale","Fulton","480-2418 Nec Ave","99164","Dunstable","ultricies.ornare@icloud.com","1971-09-20","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Owen","Lucy","1308 Augue St.","39550","Warwick","ac.mattis@aol.couk","1974-01-11","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Buckner","Xanthus","P.O. Box 832, 2473 Varius St.","94042","Hudiksvall","scelerisque.lorem.ipsum@yahoo.couk","1992-12-10","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Blackwell","Jolene","835-6011 Ac Ave","75431","Sheikhupura","pellentesque.habitant@aol.org","1984-07-18","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1"),
  ("Rivera","Berk","Ap #509-9099 Nec Av.","64086","Mandai","nunc@icloud.ca","1997-08-26","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Little","Jameson","Ap #750-7213 Lectus Ave","38815","San Miguel","luctus.ipsum.leo@aol.edu","1981-12-26","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","0"),
  ("Zimmerman","Brittany","590-6023 Dignissim Av.","58916","Cherain","tempus.risus@google.net","1974-09-22","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Hale","Pamela","P.O. Box 672, 1777 Enim St.","63655","Hudson Bay","magnis.dis@hotmail.edu","1976-06-09","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Sheppard","Jana","Ap #537-3604 Pharetra. Rd.","36064","Richmond","semper.tellus@aol.couk","1988-03-04","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1");
INSERT INTO `member` (`surname`,`name`,`address`,`zip_code`,`city`,`email`,`birth_date`,`password`,`admin`,`validity`)
VALUES
  ("Dorsey","Aiko","Ap #407-1738 Non, Ave","42968","Vinh","lectus@google.ca","1970-12-10","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Sherman","Kirby","6530 Sed Street","86696","Chandigarh","nam@yahoo.org","1991-09-13","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","0"),
  ("Herring","Jaquelyn","Ap #558-9347 Euismod St.","60573","Mazenzele","curabitur.massa@icloud.net","1979-02-23","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Castaneda","Ronan","796-6490 Lacus. St.","38100","Serik","est.congue@aol.ca","1973-02-20","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Salinas","Macey","Ap #941-8013 Pede, Rd.","47417","Igboho","cubilia.curae@icloud.org","1978-01-20","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","0"),
  ("Vaughn","Charlotte","P.O. Box 340, 2155 Tempus Road","13318","Le Grand-Quevilly","dolor.sit.amet@protonmail.edu","1974-02-26","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","1","1"),
  ("Pennington","Judith","330-701 Amet Avenue","28589","Starodub","cum.sociis@google.org","1978-12-07","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Henderson","Cecilia","Ap #579-9476 Iaculis Av.","41452","Covington","nunc.ac@aol.com","1991-06-09","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Underwood","India","Ap #583-3721 Lectus Avenue","63664","Mannheim","interdum.sed.auctor@hotmail.com","1983-08-08","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Patton","Callum","375-7152 Sapien, Road","41758","Frutillar","ligula.aenean@google.com","1971-11-01","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1");
INSERT INTO `member` (`surname`,`name`,`address`,`zip_code`,`city`,`email`,`birth_date`,`password`,`admin`,`validity`)
VALUES
  ("Grimes","Reese","Ap #436-7121 Adipiscing St.","13352","Villa del Rosario","sollicitudin.commodo@icloud.couk","1972-08-16","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Ferrell","Bertha","356-7523 Enim. Road","44250","Launceston","ornare@icloud.org","1978-01-31","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Mcfarland","Orla","313-3168 Arcu. Street","64519","Cajamarca","vulputate.eu@yahoo.org","1972-08-12","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","1","1"),
  ("Everett","Valentine","Ap #592-8611 Malesuada Rd.","41748","Weiz","semper.nam.tempor@google.edu","1978-10-08","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Ball","Dieter","6692 Lacus. Rd.","30135","Burhanpur","ut.tincidunt@google.ca","1989-03-13","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1"),
  ("Durham","Justine","135-7328 Malesuada Rd.","99888","Oudtshoorn","ac.sem@protonmail.ca","1988-07-21","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Fitzgerald","Miranda","4275 Dolor Rd.","14814","Uruapan","sociis@outlook.org","1991-12-13","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Anthony","Bruno","559-3431 Nam Rd.","51544","Cao Lãnh","pharetra.sed@protonmail.com","1992-09-16","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Fowler","Nathan","4716 Ut St.","95281","Tarnów","pellentesque@google.net","1977-12-15","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Vega","Price","3249 Leo. Street","52662","Maryborough","non.dui@aol.com","1978-06-15","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1");
INSERT INTO `member` (`surname`,`name`,`address`,`zip_code`,`city`,`email`,`birth_date`,`password`,`admin`,`validity`)
VALUES
  ("Hicks","Evangeline","688-5030 Arcu. Road","34138","Chungju","dignissim.tempor@icloud.net","1973-02-10","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Howard","Serena","5248 Tellus St.","03573","Rudiano","felis.eget@aol.org","1970-08-12","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Hutchinson","Austin","998-7449 Eleifend. St.","55303","Limoges","neque.pellentesque@protonmail.com","1985-12-28","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Berger","Kelsie","P.O. Box 411, 8774 Egestas. Rd.","73012","Dunoon","sed@icloud.ca","1976-12-26","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Collink","Jonah","822-4736 Per Street","39613","Chuncheon","commodo.at.libero@google.net","1993-03-30","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1"),
  ("Hendricks","Ivana","P.O. Box 915, 8971 Pellentesque St.","41782","Mexico City","sed.pharetra.felis@yahoo.org","1997-11-19","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Sanford","Dante","Ap #908-1762 Risus. Avenue","11156","Alexandra","pede.sagittis@aol.net","1977-10-06","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","0"),
  ("Wallace","Isabella","253-9967 Vivamus Street","88583","Blenheim","eget.ipsum@yahoo.edu","1976-05-20","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Cross","Hyacinth","Ap #502-2581 Interdum Rd.","87429","Ely","quis.urna.nunc@yahoo.couk","1994-10-15","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Greer","Bruce","427-8662 Semper St.","27635","Blue Mountains","nonummy.ac@aol.org","1982-07-02","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1");
INSERT INTO `member` (`surname`,`name`,`address`,`zip_code`,`city`,`email`,`birth_date`,`password`,`admin`,`validity`)
VALUES
  ("Atkins","Branden","P.O. Box 451, 5921 Egestas. Rd.","67707","Barranca","adipiscing.elit@yahoo.net","1997-09-28","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Morin","Allegra","Ap #356-9975 Aliquam, Street","35308","Ajaccio","penatibus.et@icloud.ca","1990-03-25","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Daniel","Melvin","7616 Praesent St.","85562","Ipiales","dolor@yahoo.net","1983-02-05","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Wilder","Dolan","426 Nam Rd.","75270","Dumai","consectetuer.mauris@hotmail.ca","1985-06-18","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Wilkerson","Catherine","Ap #278-5440 Sociis Ave","21573","Lviv","lobortis@google.edu","1983-07-17","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","1","1"),
  ("Mcclain","Jorden","Ap #177-7239 Scelerisque St.","22292","Manokwari","molestie.arcu@yahoo.ca","1986-10-06","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Sparks","Candace","Ap #283-6968 Duis Avenue","52412","Castres","nec.luctus@outlook.ca","1982-10-15","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Heath","Preston","P.O. Box 433, 6766 Tortor. Street","23213","Puerto Colombia","adipiscing@yahoo.ca","1973-07-22","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","1","0"),
  ("Page","Imogene","Ap #797-7805 Lacus. Rd.","64530","Nazilli","accumsan.neque.et@google.edu","1984-07-21","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","1","1"),
  ("Alston","Aladdin","Ap #271-3426 Curae St.","35018","Waiheke Island","at.velit@outlook.edu","1996-06-09","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1");
INSERT INTO `member` (`surname`,`name`,`address`,`zip_code`,`city`,`email`,`birth_date`,`password`,`admin`,`validity`)
VALUES
  ("Fernandez","James","Ap #494-9900 Vestibulum, Av.","65906","Buguma","sollicitudin.a@outlook.couk","1970-09-20","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Cash","Dorothy","P.O. Box 972, 2460 Dolor Avenue","92505","Walsall","ipsum.sodales.purus@protonmail.net","1983-01-13","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Freeman","Vera","Ap #320-1834 Egestas Rd.","97821","Andong","eros.nam@outlook.ca","1994-10-24","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Potter","Barry","720-237 Velit Avenue","40903","Tuy Hòa","orci.lobortis@outlook.org","1993-12-12","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Chen","Dawn","7070 Velit. St.","35669","Kovel","dignissim.tempor.arcu@google.couk","1997-04-04","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1"),
  ("Emerson","Nathan","Ap #617-4550 Nisi St.","44304","Bannu","sed.dictum.eleifend@yahoo.couk","1971-01-09","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","1","1"),
  ("Franco","Vincent","4364 Cum Avenue","01563","Bünyan","in.scelerisque@protonmail.edu","1992-02-26","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Cunningham","Chava","Ap #379-5558 Et Road","15158","Tapachula","erat.vitae@aol.edu","1990-10-18","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Huber","Jayme","Ap #610-4737 Orci, Rd.","34795","Volgograd","malesuada.vel.venenatis@google.ca","1980-02-27","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Ferrell","Francis","818-8621 Iaculis St.","83738","Tarbes","luctus.vulputate@hotmail.org","1989-01-12","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1");
INSERT INTO `member` (`surname`,`name`,`address`,`zip_code`,`city`,`email`,`birth_date`,`password`,`admin`,`validity`)
VALUES
  ("Morgan","Lance","1207 Ut Avenue","38966","Sóc Trăng","lobortis.quis.pede@aol.edu","1981-08-15","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Elliott","Hope","P.O. Box 656, 1764 Eu Av.","46681","Nicoya","pellentesque.ultricies@yahoo.couk","1974-07-10","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Kirkland","Shad","Ap #279-5305 Ac Ave","73712","Smolensk","non.enim@aol.edu","1998-06-03","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Douglas","Cody","859-696 Curae Avenue","22416","Balfour","libero.mauris@outlook.couk","1997-10-16","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Blackburn","Shaeleigh","P.O. Box 166, 6795 Eget Road","96366","Thứa","non.sapien@outlook.org","1982-05-10","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1"),
  ("Green","Vanna","926-3309 Eros. St.","70913","North Berwick","lorem.ut.aliquam@google.edu","1987-06-23","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Stuart","Lars","298-2512 Urna. Avenue","31233","Mora","odio@protonmail.org","1982-04-20","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","0"),
  ("Morrison","Emi","804-7966 Amet, St.","08481","Musakhel","nec@hotmail.com","1975-10-31","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Rollins","Kevin","101-4897 Eget Road","25518","Hofors","mus.proin.vel@yahoo.edu","1978-06-21","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Blankenship","Xyla","Ap #243-9021 Sed St.","21579","Gontrode","metus@aol.net","1972-07-20","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1");
INSERT INTO `member` (`surname`,`name`,`address`,`zip_code`,`city`,`email`,`birth_date`,`password`,`admin`,`validity`)
VALUES
  ("Goodwin","Vaughan","288-8236 Proin Ave","85650","Muradiye","tincidunt@yahoo.couk","1994-05-17","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Weber","Merrill","6496 Dui Rd.","32405","Rustenburg","sed@outlook.com","1977-09-06","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Britt","Blake","Ap #425-9787 Curabitur Ave","41486","Braies/Prags","ipsum.primis@yahoo.net","1977-08-09","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Owens","Griffith","740 Mollis. St.","45379","Zittau","proin@protonmail.edu","1986-10-01","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","0"),
  ("Velazquez","Allegra","P.O. Box 199, 7434 Enim. Rd.","72712","Miryang","lorem@hotmail.com","1983-07-20","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1"),
  ("Rhodes","Bryar","Ap #688-5642 Mauris St.","32814","Białystok","ridiculus.mus@yahoo.com","1998-11-03","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Jefferson","Abdul","Ap #254-5367 Cum Av.","35682","Mandaue","vitae.orci@outlook.edu","1987-07-20","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Stafford","Prescott","P.O. Box 640, 1321 Ultrices, Rd.","46813","Smithers","sagittis.felis@aol.edu","1992-05-22","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","0"),
  ("Clark","Yuli","5610 Donec Ave","16257","Huế","enim.sit@hotmail.edu","1989-12-29","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Sandoval","Urielle","Ap #139-365 Duis Ave","63653","Randburg","lorem.ut@yahoo.com","1988-11-03","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1");
INSERT INTO `member` (`surname`,`name`,`address`,`zip_code`,`city`,`email`,`birth_date`,`password`,`admin`,`validity`)
VALUES
  ("Cardenas","Sigourney","Ap #861-6566 Diam Av.","93170","Wrocław","placerat.eget@icloud.net","1986-02-16","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","0"),
  ("Gillespie","Wynter","563-5738 Risus. St.","93640","Whangarei","amet.faucibus@protonmail.ca","1993-12-01","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Morgan","Beverly","4513 Ante St.","76708","Neiva","suspendisse.dui@hotmail.com","1982-12-14","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Jennings","Kasper","Ap #450-7664 Arcu. Av.","08353","Arequipa","eget.massa.suspendisse@yahoo.couk","1979-05-12","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Collink","Eve","P.O. Box 898, 6122 Diam Avenue","84149","Drancy","facilisis.lorem@google.edu","1984-12-21","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1"),
  ("Olsen","Amena","5109 Adipiscing Street","07850","Penza","nullam.suscipit@protonmail.ca","1998-01-11","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Dennis","Meghan","Ap #696-1041 Congue Ave","65199","Chancay","dolor.nulla@protonmail.com","1982-07-09","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Brewer","Jason","6947 Rutrum Rd.","02397","Newtonmore","dignissim@google.net","1990-08-23","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Lynch","Richard","937-2192 Magna. St.","40054","Los Mochis","scelerisque.neque.sed@outlook.edu","1970-09-28","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","1","1"),
  ("Mcclure","Driscoll","Ap #449-5877 Euismod Avenue","52146","Guaitecas","non.nisi@protonmail.com","1972-10-14","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1");
INSERT INTO `member` (`surname`,`name`,`address`,`zip_code`,`city`,`email`,`birth_date`,`password`,`admin`,`validity`)
VALUES
  ("Coleman","Doris","3900 Ut Ave","79422","Roosendaal","dolor.sit.amet@yahoo.org","1985-10-12","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Bruce","Hu","979-1638 Ligula Av.","82476","Haugesund","integer.eu@aol.org","1989-11-08","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Bennett","Callie","P.O. Box 904, 6458 Tempus Rd.","51352","San José de Alajuela","eget.venenatis@yahoo.com","1980-08-23","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Burton","Lunea","512-2269 Pellentesque Av.","67828","Vallentuna","dictum.mi@google.org","1998-09-26","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Melton","Prescott","7685 Ultricies St.","53634","Stockholm","consectetuer.rhoncus.nullam@outlook.edu","1970-07-23","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1"),
  ("Delacruz","Giacomo","Ap #901-8323 Mi, Avenue","32820","Tehuacán","sit.amet@outlook.ca","1998-09-10","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Stuart","Shoshana","Ap #948-7970 Mi Rd.","31378","Aguazul","donec@google.edu","1987-06-01","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Estrada","Tamara","8710 Pretium Rd.","17754","Rivière-du-Loup","tellus.nunc@protonmail.net","1977-02-24","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Bennett","May","3797 Cursus St.","48774","Penrith","augue@hotmail.ca","1992-05-20","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Mason","Beverly","P.O. Box 880, 2687 Parturient Avenue","56297","Kaduna","semper.auctor.mauris@hotmail.com","1987-12-09","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1");
INSERT INTO `member` (`surname`,`name`,`address`,`zip_code`,`city`,`email`,`birth_date`,`password`,`admin`,`validity`)
VALUES
  ("Lindsey","Armando","Ap #108-4731 Mollis. Street","82564","Lede","mauris.a@yahoo.org","1972-11-27","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","1","1"),
  ("Rivers","Ulric","838-5438 Ipsum. St.","44685","Aurillac","porta@protonmail.ca","1975-11-05","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Vang","Eric","Ap #229-4770 Mauris, Ave","83173","Biała Podlaska","purus.gravida@aol.couk","1996-03-18","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Perez","Dolan","447-132 Interdum Road","52646","Parauapebas","eget@google.com","1985-07-08","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Torres","Baker","Ap #480-5760 Ligula. Rd.","40036","Ryazan","vestibulum@protonmail.couk","1972-09-12","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1"),
  ("Baird","Nora","P.O. Box 586, 2685 Egestas Av.","98663","Sant'Eufemia a Maiella","sed.tortor@protonmail.couk","1979-03-27","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Stone","Harlan","Ap #523-2969 Magna. Street","90407","Baranello","justo.nec@aol.edu","1976-05-22","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Delaney","Hoyt","583-3176 Ligula Rd.","28519","Baracaldo","proin.non.massa@aol.org","1998-11-29","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Lowe","Kim","2207 Arcu. Street","40889","Mocoa","eros@hotmail.com","1985-04-08","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Bentley","Willow","Ap #777-3183 Interdum St.","10788","Elen","sem.ut@protonmail.org","1983-12-24","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1");
INSERT INTO `member` (`surname`,`name`,`address`,`zip_code`,`city`,`email`,`birth_date`,`password`,`admin`,`validity`)
VALUES
  ("Watson","Whilemina","345-7552 Erat. Road","26171","Randazzo","dolor@hotmail.net","1996-02-03","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Sutton","Calvin","Ap #376-7429 Lacus. St.","64877","Knoxcity","sit.amet@yahoo.com","1971-11-15","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Morin","Levi","Ap #692-8575 Sodales Road","51396","Schwalbach","vel@icloud.net","1982-12-11","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Hull","Althea","300-4184 Lorem, Road","14064","Sapele","nulla@google.org","1971-11-12","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("O'brien","Chanda","186-5739 Tempor Ave","88131","Trà Vinh","ultrices@protonmail.ca","1986-09-04","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1"),
  ("Nicholson","Cameran","351-3244 Dolor Av.","67748","Istanbul","nullam@google.ca","1994-09-10","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Greene","Elijah","160-3967 In Av.","08142","Veracruz","convallis.est.vitae@icloud.edu","1972-02-21","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Chan","Sylvia","339-6112 Purus. Avenue","36387","Champorcher","morbi@outlook.couk","1971-06-24","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","1","1"),
  ("Huffman","TaShya","9478 Ornare. St.","73344","Rechnitz","ante.iaculis@aol.edu","1990-01-04","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Roberson","Reuben","Ap #226-8943 Montes, Avenue","88943","Hudiksvall","augue.id@yahoo.ca","1972-02-04","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1");
INSERT INTO `member` (`surname`,`name`,`address`,`zip_code`,`city`,`email`,`birth_date`,`password`,`admin`,`validity`)
VALUES
  ("Nguyen","Ronan","737 Egestas St.","37577","Cavaso del Tomba","urna.nec.luctus@hotmail.couk","1970-12-11","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Spence","Zephania","295-1521 Orci. Ave","40154","Bogo","nec.mollis@google.ca","1991-10-30","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Davidson","Samuel","486-4054 Imperdiet Avenue","54934","Beervelde","mattis@google.org","1985-10-06","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Valdez","Basil","271 Mattis Rd.","94680","Kaliningrad","lobortis@outlook.org","1986-10-08","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Dominguez","Sylvia","Ap #970-860 Etiam Rd.","21244","Dutse","sem.semper.erat@aol.edu","1991-03-18","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1"),
  ("Fuller","Whoopi","709-6591 Orci. Rd.","21557","Yeosu","venenatis.a.magna@hotmail.ca","1992-10-22","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Myers","Ray","279-8539 Erat St.","49706","Buguma","lobortis.mauris@hotmail.edu","1985-06-17","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Rollins","Quinn","6321 Nulla Rd.","57538","Toruń","et.malesuada@yahoo.com","1971-07-03","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Watts","Marcia","Ap #493-9801 Non Ave","44186","Ibagué","habitant.morbi@outlook.org","1998-01-06","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Hampton","Kylee","7325 Suspendisse St.","04452","Korba","vestibulum.neque@aol.couk","1990-09-20","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1");
INSERT INTO `member` (`surname`,`name`,`address`,`zip_code`,`city`,`email`,`birth_date`,`password`,`admin`,`validity`)
VALUES
  ("Copeland","Elton","Ap #670-5403 Netus Rd.","02621","Tongyeong","lectus.justo@icloud.edu","1993-08-03","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Mcclure","Ursa","Ap #918-4074 Convallis St.","57249","Panguipulli","non@icloud.org","1988-12-26","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Morin","Cheyenne","417-4873 Lectus Road","60444","Montague","nibh@yahoo.couk","1996-02-16","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Sharp","Igor","888-5185 Sed Rd.","88741","Cao Lãnh","quam.dignissim@aol.net","1979-07-05","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Schroeder","Keegan","778-573 Dolor Avenue","28805","Hunan","laoreet.ipsum@outlook.net","1979-08-21","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","1","1"),
  ("Gardner","Pamela","P.O. Box 981, 5235 Nulla Rd.","32887","Güssing","aliquet.metus@google.edu","1992-11-08","$2y$10$eKtCsS.HZBOalmUxWJD2OuIKtLihvA6HPVehZ8iJCoy3a9iLKPUTy","0","1"),
  ("Aguirre","Mannix","8962 Aliquam Street","25911","Fort Laird","ultrices.mauris@icloud.com","1993-08-27","$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma","0","1"),
  ("Lara","Zelenia","997-5231 Nulla Rd.","66060","San Francisco","quisque.porttitor@aol.net","1988-05-31","$2a$14$5pYeAIhrJ73IN.3Lq3RDZ.bOQz87hbDW2bZ8aIJxfe/DhamnirMQ6","0","1"),
  ("Bryant","Maile","Ap #987-7984 In Ave","37375","Deventer","ac@google.ca","1978-05-14","$2a$08$hi1Pl8W.rnpajtLiC.jlaOMtoIRuBowKsO4ExC4y8Oe1dDT73mRUq","0","1"),
  ("Holt","Murphy","Ap #971-199 Erat. Rd.","84438","Pioneer","mollis.non@yahoo.couk","1975-08-24","$2a$10$HpirmVPHU.WoTVUUyql8dOb6GV7gS0gyzxemhU5sBKE2cazwpFLky","0","1");
