CREATE USER 'ecojetonsU'@'localhost' IDENTIFIED BY 'password';

CREATE DATABASE IF NOT EXISTS ecojetonsD
CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

GRANT ALL PRIVILEGES ON ecojetonsD.* TO 'ecojetonsU'@'localhost';

FLUSH PRIVILEGES;
