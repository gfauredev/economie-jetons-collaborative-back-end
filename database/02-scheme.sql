CREATE TABLE child(
	`id_child` INT AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL,
	`surname` VARCHAR(50) NOT NULL,
	`birth_date` DATE NOT NULL,
	`token` VARCHAR(300) DEFAULT "no_img.png",
	PRIMARY KEY(`id_child`),
	CONSTRAINT u_children_invitation_code UNIQUE(`invitation_code`)
);

CREATE TABLE member(
	`id_member` INT AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL,
	`surname` VARCHAR(50) NOT NULL,
	`birth_date` DATE NOT NULL,
	`email` VARCHAR(50) NOT NULL,
	`password` CHAR(60) NOT NULL,
	`address` VARCHAR(100),
	`zip_code` CHAR(5),
	`city` VARCHAR(50),
	`validity` BOOLEAN DEFAULT 0,
	`admin` BOOLEAN DEFAULT 0,
	PRIMARY KEY(`id_member`),
	CONSTRAINT u_member_email UNIQUE(`email`)
);

INSERT INTO member(`id_member`, `name`, `surname`, `birth_date`, `email`, `password`, `admin`, `validity`) VALUES 
(1, "administratrice", "administrateur", "1970-01-01", "admin@ecojetons", "$2a$12$Qz6ws.G4LVKZuNQC9xRVMe6yCCjCrhp5x8kT2Bs0EMKrW/LZrJ5ma", 1, 1);

CREATE TABLE objective(
	`id_objective` INT AUTO_INCREMENT,
	`entitled` VARCHAR(50) NOT NULL,
	`nbr_token` TINYINT NOT NULL,
	`duration` DOUBLE NOT NULL,
	`priority` INT NOT NULL,
	`image` VARCHAR(300) DEFAULT "no_img.png",
	`worked` BOOLEAN DEFAULT 0,
	`id_member` INT NOT NULL,
	`id_child` INT NOT NULL,
	PRIMARY KEY(`id_objective`),
	FOREIGN KEY(`id_member`) REFERENCES `member`(`id_member`) ON DELETE CASCADE,
	FOREIGN KEY(`id_child`) REFERENCES `child`(`id_child`) ON DELETE CASCADE
);

CREATE TABLE message(
	`id_message` INT AUTO_INCREMENT,
	`subject` VARCHAR(50) NOT NULL,
	`body` TEXT NOT NULL,
	`timestamp` DATETIME NOT NULL,
	`id_objective` INT NOT NULL,
	`id_member` INT NOT NULL,
	PRIMARY KEY(`id_message`),
	FOREIGN KEY(`id_objective`) REFERENCES `objective`(`id_objective`) ON DELETE CASCADE,
	FOREIGN KEY(`id_member`) REFERENCES `member`(`id_member`) ON DELETE CASCADE
);

CREATE TABLE reward(
	`id_reward` INT AUTO_INCREMENT,
	`entitled` VARCHAR(50) NOT NULL,
	`description` TEXT NOT NULL,
	`image` VARCHAR(300) DEFAULT "no_img.png",
	`id_child` INT NOT NULL,
	PRIMARY KEY(`id_reward`),
	FOREIGN KEY(`id_child`) REFERENCES `child`(`id_child`) ON DELETE CASCADE
);

CREATE TABLE role(
	`id_role` INT AUTO_INCREMENT,
	`entitled` VARCHAR(30),
	PRIMARY KEY(`id_role`)
);

INSERT INTO role (`id_role`, `entitled`) VALUES
(1, "coordinateur"),
(2, "défaut");

CREATE TABLE follow(
	`id_child` INT,
	`id_member` INT,
	`date_request` DATE,
	`id_role` INT,
	`validity` BOOLEAN DEFAULT 0,
	PRIMARY KEY(`id_child`, `id_member`),
	FOREIGN KEY(`id_child`) REFERENCES `child`(`id_child`) ON DELETE CASCADE,
	FOREIGN KEY(`id_role`) REFERENCES `role`(`id_role`) ON DELETE SET NULL,
	FOREIGN KEY(`id_member`) REFERENCES `member`(`id_member`) ON DELETE CASCADE
);

CREATE TABLE put_token(
	`id_objective` INT,
	`id_situation` INT NOT NULL,
	`timestamp` DATETIME,
	`add` BOOLEAN DEFAULT 1 NOT NULL,
	`id_member` INT NOT NULL,
	PRIMARY KEY(`id_objective`, `timestamp`),
	FOREIGN KEY(`id_objective`) REFERENCES `objective`(`id_objective`) ON DELETE CASCADE,
	FOREIGN KEY(`id_member`) REFERENCES `member`(`id_member`) ON DELETE CASCADE
);

CREATE TABLE link(
	`id_objective` INT,
	`id_reward` INT,
	PRIMARY KEY(`id_objective`, `id_reward`),
	FOREIGN KEY(`id_objective`) REFERENCES `objective`(`id_objective`) ON DELETE CASCADE,
	FOREIGN KEY(`id_reward`) REFERENCES `reward`(`id_reward`) ON DELETE CASCADE
);
