# Application Économie de Jetons Collaborative

## Description
Cette application a pour but de numeriser le système actuel de l'assotiation Trisomie 21 Toulouse, facilitant ainsi la gestion des adhérants et la notation des enfants.

## GitLab
Liens des dépots git contenant respectivement le back-end et le front-end du projet.

[Back-end / api](https://gitlab.com/ecojetons/economie-jetons-collaborative-back-end)

[Front-end](https://gitlab.com/ecojetons/economie-jetons-collaborative-front-end)

[Documentation](https://gitlab.com/ecojetons/economie-jetons-collaborative-documentation)

## Trello
Lien des Trello.

[Trello S3](https://trello.com/b/F3GRxtpx/backlogdeproduit)

[Trello S4](https://trello.com/b/HUoaczkB/s4)

## Charte de programmation

1. Tous les noms en snake_case (tables, colonnes, méthodes, variables …)
1. … sauf les classes qui sont en CamelCase (première lettre majuscule)
1. Tous les noms sont au singulier, sauf les tableaux (Arrays) qui sont au pluriel
1. Tous les noms et commentaires sont en Anglais
1. Les tabulations sont préférés aux espaces pour l’indentation
