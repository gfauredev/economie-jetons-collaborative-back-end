FROM php:8.2-apache

RUN a2enmod rewrite

RUN mkdir -p /etc/ecojeton
RUN mkdir -p /var/log/ecojeton

RUN cat <<EOF > /etc/ecojeton/config.json
"DATABASE":{ "SGBD":$DB_SGBD, "HOST":$DB_NAME, "DBNAME":$DB_NAME, "DBUSER":$DB_USER, "DBPASS":$DB_PASS }
EOF

COPY root.htaccess /var/www/html/.htaccess

COPY class/ /var/www/html/class/
COPY api/ /var/www/html/api/

WORKDIR /var/www/html

RUN chown --recursive www-data ./*
RUN chown www-data .htaccess

EXPOSE 80
