<?php

class Database
{
	private static $_instance;
	private $_connection;

	private function __construct()
	{
		if (!is_readable("/etc/ecojeton/config.json")) {
			throw new ExceptionUnreadableConfig();
		}
		if (!$conf_jason = file_get_contents("/etc/ecojeton/config.json")) {
			throw new ExceptionUnreadableConfig();
		}

		$conf = json_decode($conf_jason);
		$SGBD = $conf['database']['SGBD'];
		$HOST = $conf['database']['HOST'];
		$DBNAME = $conf['database']['DBNAME'];
		$DBUSER = $conf['database']['DBUSER'];
		$DBPASS = $conf['database']['DBPASS'];


		$this->_connection = new PDO($SGBD.":host=".$HOST.";dbname=".$DBNAME, $DBUSER, $DBPASS);
	}

	public static function get_instance() : Database
	{
		if (!self::$_instance) {
			self::$_instance = new Database();
		}
		return self::$_instance;
	}

	public function get_connection() : PDO
	{
		return $this->_connection;
	}

}
