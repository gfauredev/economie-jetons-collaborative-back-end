<?php

function deliver_response($status, $status_message, $data)
{
	header("HTTP/1.1 $status $status_message");
	$response['status'] = $status;
	$response['status_message'] = $status_message;
	$response['data'] = $data;
	$json_response = json_encode($response);
	echo $json_response;
}

function is_admin(int $id) : bool
{
	return false;
}

function get_member_id(array $token) : int
{
	return $token['id_member'];
}