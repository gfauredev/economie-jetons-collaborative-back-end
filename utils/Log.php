<?php

class Log
{
	private static $_application_log = "/var/log/ecojeton/application.txt";
	private static $_connection_log = "/var/log/ecojeton/connection.txt";
	private static $_error_log = "/var/log/ecojeton/error.txt";

	private static function write_log(string $string, string $file): void
	{
		error_log($string, 3, $file);
	}

	public static function write_log_connection(bool $state, string $email): void
	{
		$timestamp = date("Y-m-d H:i:s");
		$ip_client = $_SERVER['REMOTE_ADDR'];
		self::write_log("[".$timestamp."]"."IP=".$ip_client.",STATUS=".$state.",\n", self::$_connection_log);
	}

	public static function write_log_error(string $action, string $error_message, int $error_code): void
	{
		$timestamp = date("Y-m-d H:i:s");
		$id_member = $_SESSION['id_member'];
		self::write_log("[".$timestamp."]"."USER=".$id_member.",ACTION=".$action."ERROR_MESSAGE=".$error_message."ERROR_CODE=".$error_code, self::$_error_log);
	}

	public static function write_log_application(string $action): void
	{
		$timestamp = date("Y-m-d H:i:s");
		$id_member = $_SESSION['id_member'];
		self::write_log("[".$timestamp."]"."USER=".$id_member.",ACTION=".$action, self::$_application_log);
	}
}
