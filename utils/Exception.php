<?php

class MyException extends Exception
{
	public function __construct(string $type, string $error, int $error_code)
	{
		Log::write_log_error($type, $error, $error_code);
		parent::__construct($error, $error_code);
	}
}

class ExceptionRessourceNotFound extends MyException
{
	public function __construct()
	{
		parent::__construct("a", "Ressources not found", 404);
	}
}

class ExceptionDatabase extends MyException
{
	public function __construct()
	{
		parent::__construct("a", "Error on database", 500);
	}
}

class ExceptionLoginRequire extends MyException
{
	public function __construct()
	{
		parent::__construct("a", "Login require", 401);
	}
}

class ExceptionIssuficiantPermission extends MyException
{
	public function __construct()
	{
		parent::__construct("a", "insuficiant permission", 403);
	}
}

class ExceptionRessourceExist extends MyException
{
	public function __construct()
	{
		parent::__construct("a", "Ressource exist", 400);
	}
}

class ExceptionAddToken extends MyException
{
	public function __construct()
	{
		parent::__construct("a", "Impossible to add a token", 400);
	}
}

class ExceptionDelToken extends MyException
{
	public function __construct()
	{
		parent::__construct("a", "impossible to delete a token", 400);
	}
}

class ExceptionMissingParameter extends MyException
{
	public function __construct()
	{
		parent::__construct("a", "missing parameters", 400);
	}
}

class ExceptionUnreadableConfig extends MyException
{
	public function __construct()
	{
		parent::__construct("a", "unreadable config", 500);
	}
}