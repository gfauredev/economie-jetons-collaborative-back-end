<?php

require_once("functions.php");
require_once("jwt_utils.php");

header("Content-Type:application/json");
$http_method = $_SERVER['REQUEST_METHOD'];

$bearer_token = get_bearer_token();
if (is_jwt_valid($bearer_token, "pass")) { $user = json_decode($bearer_token); }
else { deliver_response(403, "need to be connnected", null); die(); }

switch ($http_method) {

	/**
	 * Code bloc for the GET method on children ressouce
	 * all -> require to be admin
	 * all, coordinated, default -> require to be connected 
	*/
	case "GET" :
		try {

			if (empty($_GET['id_child'])) {
				switch ($_GET['val']) {
					case 'follow':
						$result = Member::get_children_followed($id_member); // ! need to replace $id_member
						break;
					
					case 'coordinated':
						$result = Member::get_children_coordinated($id_member); // ! need to replace $id_member
						break;
					
					default:
						if (!is_admin($user)) { throw new ExceptionIssuficiantPermission(); }
						$result = Child::get_all();
						break;
				}
			} else {
				if (!Member::is_linked_to(get_member_id($user), $_GET['id_child']) || !is_admin($user))
				{
					throw new ExceptionIssuficiantPermission();
				}
				$result = Child::get($_GET['id_child']);
			}

			$matchingData = json_encode($result);
			$RETURN_CODE = 200;
			$STATUS_MESSAGE = "Successfully obtained";
		} catch (\Throwable $th) {
			$RETURN_CODE = $th->getCode();
			$STATUS_MESSAGE = $th->getMessage();
			$matchingData = null;
		} finally {
			deliver_response($RETURN_CODE, $STATUS_MESSAGE, $matchingData);
		}
		break;

	/**
	 * Code bloc for the POST method on children ressource
	 * -> require to be admin
	*/
	case "POST" :
		try {
			if (!is_admin($user)) { throw new ExceptionIssuficiantPermission(); }

			$postedData = file_get_contents('php://input');
			$data=json_decode($postedData, true);
		
			if (empty($data['args']) && empty($data['args2'])) {
				throw new ExceptionMissingParameter();
			}

			$result = Child::create();
			$matchingData = json_encode($result);

			$RETURN_CODE = 201;
			$STATUS_MESSAGE = "Successfully inserted";
		} catch (\Throwable $th) {
			$RETURN_CODE = $th->getCode();
			$STATUS_MESSAGE = $th->getMessage();
			$matchingData = null;
		} finally {
			deliver_response($RETURN_CODE, $STATUS_MESSAGE, $matchingData);
		}
		break;
	
	/**
	 * Code bloc for the PUT method on children ressource
	 * -> require to be admin
	*/
	case "PUT" :
		try {
			if (!is_admin($user)) { throw new ExceptionIssuficiantPermission(); }

			if (empty($_GET['id_child'])) { throw new ExceptionMissingParameter(); }

			$postedData = file_get_contents('php://input');
			$data=json_decode($postedData, true);

			if (empty($data['args']) && empty($data['args2'])) {
				throw new ExceptionMissingParameter();
			}

			$result = Child::modify();
			$matchingData = json_encode($result);

			$RETURN_CODE = 200;
			$STATUS_MESSAGE = "Successfully updated";
		} catch (\Throwable $th) {
			$RETURN_CODE = $th->getCode();
			$STATUS_MESSAGE = $th->getMessage();
			$matchingData = null;
		} finally {
			deliver_response($RETURN_CODE, $STATUS_MESSAGE, $matchingData);
		}
		break;

	/**
	 * Code bloc for the DELETE method on children ressource
	 * -> require to be admin
	*/
	case "DELETE" :
		try {
			if (!is_admin($user)) { throw new ExceptionIssuficiantPermission(); }
			
			if (empty($_GET['id_child'])) { throw new ExceptionMissingParameter(); }

			Child::delete($_GET['id_child']);

			$RETURN_CODE = 200;
			$STATUS_MESSAGE = "Successfully deleted";
		} catch (\Throwable $th) {
			$RETURN_CODE = $th->getCode();
			$STATUS_MESSAGE = $th->getMessage();
		} finally {
			deliver_response($RETURN_CODE, $STATUS_MESSAGE, null);
		}
		break;
	
	/** */
	default :
		deliver_response(405, "not implemented method", null);
		break;
}
