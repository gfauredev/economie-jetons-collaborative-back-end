<?php

require_once("functions.php");
require_once("jwt_utils.php");

header("Content-Type:application/json");
$http_method = $_SERVER['REQUEST_METHOD'];

if (empty($_GET['id_child'])) { throw new ExceptionMissingParameter(); }

switch ($http_method) {

	case "GET" :
		try {

			if (empty($_GET['id_reward'])) {
				$result = Reward::get_all_of_child($_GET['id_child']);
			} else {
				$result = Reward::get($_GET['id_reward']);
			}

			$matchingData = json_encode($result);
			$RETURN_CODE = 200;
			$STATUS_MESSAGE = "Successfully obtained";
		} catch (\Throwable $th) {
			$RETURN_CODE = $th->getCode();
			$STATUS_MESSAGE = $th->getMessage();
			$matchingData = null;
		} finally {
			deliver_response($RETURN_CODE, $STATUS_MESSAGE, $matchingData);
		}
		break;

	case "POST" :
		try {
			$postedData = file_get_contents('php://input');
			$data=json_decode($postedData, true);

			if (!Member::is_coordinator(get_user_id($user), $_GET['id_child']) || !is_admin($token)) {
				throw new ExceptionIssuficiantPermission();
			}

			if (empty($data['entitled']) || empty($data['description']) || empty($data['image']) || empty($data['id_child'])) {
				throw new ExceptionMissingParameter();
			}

			$result = Reward::create($data['entitled'], $data['description'], $data['image'], $data['id_child'],);

			$matchingData = json_encode($result);
			$RETURN_CODE = 201;
			$STATUS_MESSAGE = "Successfully inserted";
		} catch (\Throwable $th) {
			$RETURN_CODE = $th->getCode();
			$STATUS_MESSAGE = $th->getMessage();
			$matchingData = null;
		} finally {
			deliver_response($RETURN_CODE, $STATUS_MESSAGE, $matchingData);
		}
		break;
	
	case "PUT" :
		try {
			if (empty($_GET['id_reward'])) { throw new ExceptionMissingParameter(); }

			$postedData = file_get_contents('php://input');
			$data=json_decode($postedData, true);

			if (!Member::is_coordinator(get_user_id($user), $_GET['id_child']) || !is_admin($token)) {
				throw new ExceptionIssuficiantPermission();
			}

			if (empty($data['id_reward']) || empty($data['entitled']) || empty($data['description']) || empty($data['image']) || empty($data['id_child'])) {
				throw new ExceptionMissingParameter();
			}

			$result = Reward::modify($data['id_reward'], $data['entitled'], $data['description'], $data['image']);

			$matchingData = json_encode($result);
			$RETURN_CODE = 200;
			$STATUS_MESSAGE = "Successfully updated";
		} catch (\Throwable $th) {
			$RETURN_CODE = $th->getCode();
			$STATUS_MESSAGE = $th->getMessage();
			$matchingData = null;
		} finally {
			deliver_response($RETURN_CODE, $STATUS_MESSAGE, $matchingData);
		}
		break;

	case "DELETE" :
		try {
			if (empty($_GET['id_reward'])) { throw new ExceptionMissingParameter(); }

			if (!Member::is_coordinator(get_user_id($user), $_GET['id_child']) || !is_admin($token)) {
				throw new ExceptionIssuficiantPermission();
			}

			Role::delete($_GET['id_reward']);

			$RETURN_CODE = 200;
			$STATUS_MESSAGE = "Successfully deleted";
		} catch (\Throwable $th) {
			$RETURN_CODE = $th->getCode();
			$STATUS_MESSAGE = $th->getMessage();
		} finally {
			deliver_response($RETURN_CODE, $STATUS_MESSAGE, null);
		}
		break;
	
	default :
		deliver_response(405, "not implemented method", null);
		break;
}
