<?php

require_once("functions.php");
require_once("jwt_utils.php");

header("Content-Type:application/json");
$http_method = $_SERVER['REQUEST_METHOD'];

$bearer_token = get_bearer_token();
if(is_jwt_valid($bearer_token, "pass")){ $user = json_decode($bearer_token); } 
else { deliver_response(403, "need to be connnected", null); die(); }

// TODO: check if user,objective and situation is  linked
switch ($http_method) {

	case "GET" :
		try {
			// TODO: implement the methode

			$matchingData = json_encode($result);
			$RETURN_CODE = 200;
			$STATUS_MESSAGE = "Successfully inserted";
		} catch (\Throwable $th) {
			$RETURN_CODE = $th->getCode();
			$STATUS_MESSAGE = $th->getMessage();
			$matchingData = null;
		} finally {
			deliver_response($RETURN_CODE, $STATUS_MESSAGE, $matchingData);
		}
		break;

	case "POST" :
		try {
			// TODO: implement the methode

			$matchingData = json_encode($result);
			$RETURN_CODE = 201;
			$STATUS_MESSAGE = "Successfully inserted";
		} catch (\Throwable $th) {
			$RETURN_CODE = $th->getCode();
			$STATUS_MESSAGE = $th->getMessage();
			$matchingData = null;
		} finally {
			deliver_response($RETURN_CODE, $STATUS_MESSAGE, $matchingData);
		}
		break;
	
	/** */
	default :
		deliver_response(405, "not implemented method", null);
		break;
}
