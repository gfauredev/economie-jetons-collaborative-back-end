
<?php

require_once("functions.php");
require_once("jwt_utils.php");

header("Content-Type:application/json");
$http_method = $_SERVER['REQUEST_METHOD'];

$bearer_token = get_bearer_token();
if(is_jwt_valid($bearer_token, "pass")) {
	$user = json_decode($bearer_token);
} else {
	deliver_response(403, "need to be connnected", null);
	die("error connection");
}

if (!is_admin($user)) { throw new ExceptionIssuficiantPermission(); }

switch ($http_method) {

	/**
	 * 
	*/
	case "GET" :
		try {

			if (empty($_GET['id_member'])) {
				$result = Member::get_all();
			} else {
				$result = Member::get($_GET["id_member"]);
			}

			$matchingData = json_encode($result);
			$RETURN_CODE = 200;
			$STATUS_MESSAGE = "Successfully obtained";
		} catch (\Throwable $th) {
			$RETURN_CODE = $th->getCode();
			$STATUS_MESSAGE = $th->getMessage();
			$matchingData = null;
		} finally {
			deliver_response($RETURN_CODE, $STATUS_MESSAGE, $matchingData);
		}
		break;

	/**
	 * 
	*/
	case "POST" :
		try {
			$postedData = file_get_contents('php://input');
			$data=json_decode($postedData, true);
		
			if (empty($data['name']) && empty($data['surname']) && empty($data['birthdate']) && empty($data['email']) && empty($data['password']) && empty($data['validity']) && empty($data['admin'])) {
				throw new ExceptionMissingParameter();
			}

			$result = Member::create($data['name'], $data['surname'], $data['birthdate'], $data['email'], $data['password'], $data['validity'], $data['admin'], $data['address'], $data['city'], $data['zip_code']);
			$matchingData = json_encode($result);

			$RETURN_CODE = 201;
			$STATUS_MESSAGE = "Successfully inserted";
		} catch (\Throwable $th) {
			$RETURN_CODE = $th->getCode();
			$STATUS_MESSAGE = $th->getMessage();
			$matchingData = null;
		} finally {
			deliver_response($RETURN_CODE, $STATUS_MESSAGE, $matchingData);
		}
		break;
	
	/**
	 * 
	*/
	case "PUT" :
		try {
			if (empty($_GET['id_member'])) { throw new ExceptionMissingParameter(); }

			$postedData = file_get_contents('php://input');
			$data=json_decode($postedData, true);

			if (empty($data['id_member']) && empty($data['name']) && empty($data['surname']) && empty($data['birthdate']) && empty($data['email']) && empty($data['password']) && empty($data['validity']) && empty($data['admin'])) {
				throw new ExceptionMissingParameter();
			}

			$result = Member::modify($data['id_member'], $data['name'], $data['surname'], $data['birthdate'], $data['email'], $data['password'], $data['validity'], $data['admin'], $data['address'], $data['city'], $data['zip_code']);
			$matchingData = json_encode($result);

			$RETURN_CODE = 200;
			$STATUS_MESSAGE = "Successfully updated";
		} catch (\Throwable $th) {
			$RETURN_CODE = $th->getCode();
			$STATUS_MESSAGE = $th->getMessage();
			$matchingData = null;
		} finally {
			deliver_response($RETURN_CODE, $STATUS_MESSAGE, $matchingData);
		}
		break;

	/**
	 * 
	*/
	case "DELETE" :
		try {
			if (empty($_GET['id_member'])) { throw new ExceptionMissingParameter(); }

			Member::delete($_GET['id_member']);

			$RETURN_CODE = 200;
			$STATUS_MESSAGE = "Successfully deleted";
		} catch (\Throwable $th) {
			$RETURN_CODE = $th->getCode();
			$STATUS_MESSAGE = $th->getMessage();
		} finally {
			deliver_response($RETURN_CODE, $STATUS_MESSAGE, null);
		}
		break;
	
	/** */
	default :
		deliver_response(405, "not implemented method", null);
		break;
}
