<?php

require_once("functions.php");
require_once("jwt_utils.php");

header("Content-Type:application/json");
$http_method = $_SERVER['REQUEST_METHOD'];

check_connection();

switch ($http_method) {

	case "GET" :
		try {

			if(get_role() = "admin") {

			}

			if () {}

			$matchingData = json_encode($result);
			$RETURN_CODE = 200;
			$STATUS_MESSAGE = "Successfully obtained";
		} catch (\Throwable $th) {
			$RETURN_CODE = $th->getCode();
			$STATUS_MESSAGE = $th->getMessage();
			$matchingData = null;
		} finally {
			deliver_response($RETURN_CODE, $STATUS_MESSAGE, $matchingData);
		}
		break;

	/**
	 * 
	*/
	case "POST" :
		try {
			if (!Member::is_admin()) { throw new ExceptionIssuficiantPermission(); }

			$postedData = file_get_contents('php://input');
			$data=json_decode($postedData, true);

			if (empty($data['entitled'])) { throw new ExceptionMissingParameter(); }

			$result = Role::create($data['entitled']);

			$matchingData = json_encode($result);
			$RETURN_CODE = 201;
			$STATUS_MESSAGE = "Successfully inserted";
		} catch (\Throwable $th) {
			$RETURN_CODE = $th->getCode();
			$STATUS_MESSAGE = $th->getMessage();
			$matchingData = null;
		} finally {
			deliver_response($RETURN_CODE, $STATUS_MESSAGE, $matchingData);
		}
		break;
	
	/**
	 * 
	*/
	case "PUT" :
		try {
			if (empty($_GET['id_role'])) { throw new ExceptionMissingParameter(); }

			if (!Member::is_admin()) { throw new ExceptionIssuficiantPermission(); }

			$postedData = file_get_contents('php://input');
			$data=json_decode($postedData, true);

			if (empty($data['entitled'])) { throw new ExceptionMissingParameter(); }

			$result = Role::modify($_GET['id_role'], $data['entitled']);

			$matchingData = json_encode($result);
			$RETURN_CODE = 200;
			$STATUS_MESSAGE = "Successfully updated";
		} catch (\Throwable $th) {
			$RETURN_CODE = $th->getCode();
			$STATUS_MESSAGE = $th->getMessage();
			$matchingData = null;
		} finally {
			deliver_response($RETURN_CODE, $STATUS_MESSAGE, $matchingData);
		}
		break;

	/**
	 * 
	*/
	case "DELETE" :
		try {
			if (empty($_GET['id_role'])) { throw new ExceptionMissingParameter(); }

			if (!Member::is_admin()) { throw new ExceptionIssuficiantPermission(); }

			Role::delete($_GET['id_role']);

			$RETURN_CODE = 200;
			$STATUS_MESSAGE = "Successfully deleted";
		} catch (\Throwable $th) {
			$RETURN_CODE = $th->getCode();
			$STATUS_MESSAGE = $th->getMessage();
		} finally {
			deliver_response($RETURN_CODE, $STATUS_MESSAGE, null);
		}
		break;
	
	/**
	 * 
	*/
	default :
		deliver_response(405, "not implemented method", null);
		break;
}
