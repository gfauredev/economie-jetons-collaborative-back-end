<?php

class Role
{

	public static function get(int $id_role) : array
	{
		// Neutralize
		$id_role = trim(htmlentities($id_role));

		if (!self::exist($id_role)) { throw new ExceptionRessourceNotFound(); }

		$sqlrequest = 'SELECT * FROM `role` WHERE `id_role` = :id_role';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindParam("id_role", $id_role, PDO::PARAM_INT);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
		return $request->fetch(PDO::FETCH_ASSOC);
	}

	public static function get_all() : array
	{
		$sqlrequest = 'SELECT * FROM `role`';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
		return $request->fetchAll(PDO::FETCH_ASSOC);
	}

	public static function create(string $entitled) : array
	{
		// Neutralize
		$entitled = trim(htmlentities($entitled));

		// Verify if the Child don't exist in database
		if (self::existe($entitled)) { throw new ExceptionRessourceExist(); }

		// Create the Child
		$request = Database::get_instance()->get_connection()->prepare('INSERT INTO `role`(`entitled`) VALUES(:entitled)');
		$request->bindparam('entitled', $entitled, PDO::PARAM_STR);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }

		Log::write_log_application("create role");

		return self::get(Database::get_instance()->get_connection()->lastInsertId());
	}

	public static function modify(int $id_role, string $entitled) : array
	{
		// Neutralize
		$id_role = trim(htmlentities($id_role));
		$entitled = trim(htmlentities($entitled));

		if (!self::exist($id_role)) { throw new ExceptionRessourceNotFound(); }

		$sqlrequest = 'UPDATE `role` SET `entitled` = :entitled WHERE `id_child` = :id_role';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindparam('id_role', $id_role, PDO::PARAM_INT);
		$request->bindparam('entitled', $entitled, PDO::PARAM_STR);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }

		Log::write_log_application("modify role");

		return self::get($id_role);
	}

	public static function delete(int $id_role) : void
	{
		// Neutralize
		$id_role = trim(htmlentities($id_role));

		if (!self::exist($id_role)) { throw new ExceptionRessourceNotFound(); }

		$sqlrequest = 'DELETE FROM `role` WHERE `id_role` = :id_role';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindparam('id_role', $id_role, PDO::PARAM_INT);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }

		Log::write_log_application("delete role");
	}

	public static function exist(int $id_role) : bool
	{
		$sqlrequest = 'SELECT * FROM `role` WHERE `id_role` = :id_role';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindParam("id_role", $id_role, PDO::PARAM_INT);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
		if ($request->rowCount() > 0) { return true; } else { return false; }
	}

	public static function existe(string $entitled) : bool
	{
		$sqlrequest = 'SELECT * FROM `role` WHERE `entitled` = :entitled';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindparam('entitled', $entitled, PDO::PARAM_STR);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
		if ($request->rowCount() > 0) { return true; } else { return false; }
	}

}
