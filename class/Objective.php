<?php

class Objective
{

	public static function get(int $id_objective) : array
	{
		// Neutralize
		$id_objective = trim(htmlentities($id_objective));

		// Verify if the Child don't exist in database
		if (!self::exist($id_objective)) { throw new ExceptionRessourceNotFound(); }

		$sqlrequest = 'SELECT * FROM `objective` WHERE `id_objective` = :id_objective';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindparam('id_objective', $id_objective);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
		return $request->fetch(PDO::FETCH_ASSOC);
	}

	public static function get_all_of_child(int $id_child) : array
	{
		$sqlrequest = 'SELECT * FROM `objective` WHERE `id_child` = :id_child';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindparam('id_child', $id_child, PDO::PARAM_INT);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
		return $request->fetchall(PDO::FETCH_ASSOC);
	}

	public static function get_message_of_objective(int $id_objective) : array
	{
		// Neutralize
		$id_objective = trim(htmlentities($id_objective));

		// Verify if the Child don't exist in database
		if (!self::exist($id_objective)) { throw new ExceptionRessourceNotFound(); }

		$sqlrequest = 'SELECT * FROM `message` WHERE `id_objective` = :id_objective';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindparam('id_objective', $id_objective);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
		return $request->fetchAll(PDO::FETCH_ASSOC);
	}

	public static function get_reward_of_objective(int $id_objective) : array
	{
		// Neutralize
		$id_objective = trim(htmlentities($id_objective));

		// Verify if the Child don't exist in database
		if (!self::exist($id_objective)) { throw new ExceptionRessourceNotFound(); }

		$sqlrequest = 'SELECT r.* FROM reward r, link l WHERE r.id_reward = l.id_reward AND l.id_objective = :id_objective';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindparam('id_objective', $id_objective, PDO::PARAM_INT);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
		return $request->fetchall(PDO::FETCH_ASSOC);
	}

	public static function create(string $entitled, int $nbr_token, int $duration, int $priority, int $id_member, int $id_child) : array
	{
		// Neutralize
		$entitled = trim(htmlentities($entitled));
		$nbr_token = trim(htmlentities($nbr_token));
		$duration = trim(htmlentities($duration));
		$priority = trim(htmlentities($priority));
		$id_child = trim(htmlentities($id_child));
		$id_member = trim(htmlentities($id_member));

		// Verify if the objective don't not exist in database
		if (self::existe($entitled, $nbr_token, $duration)) { throw new ExceptionRessourceExist(); }

		// Create the objective
		$sqlrequest = 'INSERT INTO `objective`(`entitled`, `nbr_token`, `duration`, `priority`, `id_member`, `id_child`) VALUES (:entitled, :nbr_token, :duration, :priority, :id_member, :id_child)';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindParam("entitled", $entitled, PDO::PARAM_STR);
		$request->bindParam("nbr_token", $nbr_token, PDO::PARAM_INT);
		$request->bindParam("duration", $duration, PDO::PARAM_INT);
		$request->bindParam("priority", $priority, PDO::PARAM_INT);
		$request->bindParam("id_member", $id_member, PDO::PARAM_INT);
		$request->bindParam("id_child", $id_child, PDO::PARAM_INT);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }

		Log::write_log_application("create objective");

		return self::get(Database::get_instance()->get_connection()->lastInsertId());
	}

	public static function modify(int $id_objective, string $entitled, int $nbr_token, int $duration, bool $worked, int $priority) : array
	{
		// Neutralize
		$id_objective = trim(htmlentities($id_objective));
		$entitled = trim(htmlentities($entitled));
		$nbr_token = trim(htmlentities($nbr_token));
		$duration = trim(htmlentities($duration));
		$worked = trim(htmlentities($worked));
		$priority = trim(htmlentities($priority));

		// Verify if the Child don't exist in database
		if (!self::exist($id_objective)) { throw new ExceptionRessourceNotFound(); }

		// Modify the objective
		$sqlrequest = 'UPDATE `objective`(`entitled`, `nbr_token`, `duration`, `worked`, `priority`) VALUES (:entitled, :nbr_token, :duration, :worked, :priority) WHERE `id_objective` = :id_objective';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindParam("entitled", $entitled, PDO::PARAM_STR);
		$request->bindParam("nbr_token", $nbr_token, PDO::PARAM_INT);
		$request->bindParam("duration", $duration, PDO::PARAM_INT);
		$request->bindParam("worked", $worked, PDO::PARAM_BOOL);
		$request->bindParam("priority", $priority, PDO::PARAM_INT);
		$request->bindParam("id_objective", $id_objective, PDO::PARAM_INT);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }

		Log::write_log_application("modify objective");

		return self::get($id_objective);
	}

	public static function delete(int $id_objective) : void
	{
		// Neutralize
		$id_objective = trim(htmlentities($id_objective));

		// Verify if the Child don't exist in database
		if (!self::exist($id_objective)) { throw new ExceptionRessourceNotFound(); }

		// Delete the objective
		$sqlrequest = 'DELETE FROM `objective` WHERE `id_objective` = :id_objective';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindParam("id_objective", $id_objective);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }

		Log::write_log_application("delete objective");
	}

	public static function get_success_rate(int $id_objective, string $date = null): float
	{
		// Neutralize
		$id_objective = trim(htmlentities($id_objective));

		// Verify if the Child don't exist in database
		if (!self::exist($id_objective)) { throw new ExceptionRessourceNotFound(); }

		$max_tokens = self::get($id_objective)['nbr_token'];

		// Get situations id_objective with timestamps of first tokens
		$sqlrequest = 'SELECT t.id_situation, min(t.timestamp) situation_timestamp FROM objective o LEFT JOIN put_token t ON o.id_objective = t.id_objective WHERE o.id_objective = :id_objective GROUP BY t.id_situation';
		$req = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$req->bindParam('id_objective', $id_objective);
		$req->execute();

		$situations = $req->fetchAll(PDO::FETCH_ASSOC);
		$success = 0;

		// For each successful situation, increase $success
		foreach ($situations as $s) {
			$req = Database::get_instance()->get_connection()->prepare('
				SELECT COUNT(t.timestamp)
				FROM put_token t
				WHERE t.id_objective = :id_objective
					AND t.id_situation = :id_situation
				');

			$req->bindParam('id_objective', $id_objective);
			$req->bindParam('id_situation', $s['id_situation']);

			$tokens = $req->fetchAll();

			if ($tokens >= $max_tokens) $success++;
		}

		return $success / count($situations);
	}

	public static function exist(int $id_objective) : bool
	{
		$sqlrequest = 'SELECT * FROM `objective` WHERE `id_objective` = :id_objective';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindParam("id_objective", $id_objective, PDO::PARAM_INT);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
		if ($request->rowCount() > 0) { return true; } else { return false; }
	}

	public static function existe(string $entitled, int $nbr_token, int $duration) : bool
	{
		$sqlrequest = 'SELECT * FROM `objective` WHERE `entitled` = :entitled AND `duration` = :duration AND `nbr_token` = :nbr_token';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindParam("entitled", $entitled, PDO::PARAM_STR);
		$request->bindParam("duration", $duration, PDO::PARAM_INT);
		$request->bindParam("nbr_token", $nbr_token, PDO::PARAM_INT);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
		if ($request->rowCount() > 0) { return true; } else { return false; }
	}
}
