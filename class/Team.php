<?php

class Team
{
	public static function get_team_of_member(int $id_member): array
	{
		$sqlrequest = 'SELECT * FROM `follow` WHERE `id_member` = :id_member';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindparam('id_member', $id_member, PDO::PARAM_INT);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
		return $request->fetchall(PDO::FETCH_ASSOC);
	}

	public static function get_team_of_child(int $id_member): array
	{
		$sqlrequest = 'SELECT * FROM `follow` WHERE `id_child` = :id_child';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindparam('id_child', $id_child, PDO::PARAM_INT);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
		return $request->fetchall(PDO::FETCH_ASSOC);
	}

	public static function add_member_to_child_team(int $id_child, int $id_member, int $id_role): array
	{
		$sqlrequest = 'INSERT INTO follow(`id_member`, `id_child`, `date_request`, `id_role`) VALUES(:id_member, :id_child, :date, :id_role)';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$date = date("Y-m-d H:i:s");
		$request->bindparam('id_child', $id_child, PDO::PARAM_INT);
		$request->bindparam('id_member', $id_member, PDO::PARAM_INT);
		$request->bindparam('id_role', $id_role, PDO::PARAM_INT);
		$request->bindparam('date', $date, PDO::PARAM_INT);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }

		return self::get_team_of_child($id_child);
	}

	public static function remove_member_to_child_team(int $id_child, int $id_member): array
	{
		$sqlrequest = 'DELETE FROM `follow` WHERE `id_member` = :id_member AND `id_child` = :id_child';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindparam('id_member', $id_member, PDO::PARAM_INT);
		$request->bindparam('id_child', $id_child, PDO::PARAM_INT);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }

		return self::get_team_of_child($id_child);
	}

	public static function member_unfollow_child(int $id_member, int $id_child): array
	{
		self::remove_member_to_child_team($id_child, $id_member);
		return self::get_team_of_member($id_member);
	}
}
