<?php

class Child
{

	public static function get(int $id_child): array
	{
		// Neutralize
		$id_child = trim(htmlentities($id_child));

		if (!self::exist($id_child)) { throw new ExceptionRessourceNotFound(); }

		$sqlrequest = 'SELECT * FROM `child` WHERE `id_child` = :id_child';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindParam("id_child", $id_child, PDO::PARAM_INT);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
		return $request->fetch(PDO::FETCH_ASSOC);
	}

	public static function get_all(): array
	{
		$sqlrequest = 'SELECT * FROM `child`';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
		return $request->fetchAll(PDO::FETCH_ASSOC);
	}

	public static function create(string $name, string $surname, $birthdate): array
	{
		// Neutralize
		$name = trim(htmlentities($name));
		$surname = trim(htmlentities($surname));
		$birthdate = trim(htmlentities($birthdate));

		// Verify if the Child don't exist in database
		if (self::existe($name, $surname, $birthdate)) { throw new ExceptionRessourceExist(); }
		
		// Create the Child
		$sqlrequest = 'INSERT INTO `child`(`name`, `surname`, `birth_date`, `invitation_code`) VALUES(:name, :surname, :birth_date, :invitation_code)';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$invitation_code = uniqid();
		$request->bindparam('name', $name, PDO::PARAM_STR);
		$request->bindparam('surname', $surname, PDO::PARAM_STR);
		$request->bindparam('birth_date', $birthdate);
		$request->bindparam('invitation_code', $invitation_code, PDO::PARAM_STR);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
		
		Log::write_log_application("create child");

		return self::get(Database::get_instance()->get_connection()->lastInsertId());
	}

	public static function modify(int $id_child, string $name, string $surname, $birthday, string $path_of_image): array
	{
		// Neutralize
		$id_child = trim(htmlentities($id_child));
		$name = trim(htmlentities($name));
		$surname = trim(htmlentities($surname));
		$birthday = trim(htmlentities($birthday));
		$path_of_image = trim(htmlentities($path_of_image));

		// Verify if the Child don't exist in database
		if (!self::exist($id_child)) { throw new ExceptionRessourceNotFound(); }

		$sqlrequest = 'UPDATE `child` SET `name` = :name, `surname` = :surname, `birth_date` = :birth_date, token` = :token WHERE `id_child` = :id_child';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindparam('id_child', $id_child, PDO::PARAM_INT);
		$request->bindparam('name', $name, PDO::PARAM_STR);
		$request->bindparam('surname', $surname, PDO::PARAM_STR);
		$request->bindparam('birth_date', $birthdate);
		$request->bindparam('token', $path_of_image, PDO::PARAM_STR);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }

		Log::write_log_application("modify child");

		return self::get($id_child);
	}

	public static function delete(int $id_child): void
	{
		// Neutralize
		$id_child = trim(htmlentities($id_child));

		// Verify if the Child don't exist in database
		if (!self::exist($id_child)) { throw new ExceptionRessourceNotFound(); }

		$sqlrequest = 'DELETE FROM `child` WHERE id_child = :id_child';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindparam('id_child', $id_child, PDO::PARAM_INT);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }

		Log::write_log_application("delete child");
	}

	public static function get_team_of_child(int $id_child): array
	{
		// Neutralize
		$id_child = trim(htmlentities($id_child));

		// Verify if the Child don't exist in database
		if (!self::exist($id_child)) { throw new ExceptionRessourceNotFound(); }

		return Team::get_team_of_child($id_child);
	}

	public static function get_objective_of_child(int $id_child): array
	{
		// Neutralize
		$id_child = trim(htmlentities($id_child));

		// Verify if the Child don't exist in database
		if (!self::exist($id_child)) { throw new ExceptionRessourceNotFound(); }

		return Objective::get_all_of_child($id_child);
	}

	public static function get_reward_of_child(int $id_child): array
	{
		// Neutralize
		$id_child = trim(htmlentities($id_child));

		// Verify if the Child don't exist in database
		if (!self::exist($id_child)) { throw new ExceptionRessourceNotFound(); }

		return Reward::get_all_of_child($id_child);
	}

	public static function new_invitation_code(int $id_child): void
	{
		$sqlrequest = 'UPDATE `child` SET `invitation_code` = :invitation_code WHERE `id_child` = :id_child';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$invitation_code = uniqid();
		$request->bindparam('id_child', $id_child, PDO::PARAM_INT);
		$request->bindparam('invitation_code', $invitation_code, PDO::PARAM_STR);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
	}

	public static function exist(int $id_child) : bool
	{
		$sqlrequest = 'SELECT * FROM `child` WHERE `id_child` = :id_child';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindParam("id_child", $id_child, PDO::PARAM_INT);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
		if ($request->rowCount() > 0) { return true; } else { return false; }
	}

	public static function existe(string $name, string $surname, $birthdate) : bool
	{
		$sqlrequest = 'SELECT * FROM `child` WHERE `name` = :name AND `surname` = :surname AND `birth_date` = :birth_date';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindparam('name', $name, PDO::PARAM_STR);
		$request->bindparam('surname', $surname, PDO::PARAM_STR);
		$request->bindparam('birth_date', $birthdate);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
		if ($request->rowCount() > 0) { return true; } else { return false; }
	}
}
