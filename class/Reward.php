<?php

class Reward
{
	public static function get($id_reward)
	{
		$id_reward = trim(htmlentities($id_reward));

		if (!self::exist($id_reward)) { throw new ExceptionRessourceNotFound(); }

		$sqlrequest = 'SELECT * FROM `reward` WHERE `id_reward` = :id_reward';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindparam('id_reward', $id_reward, PDO::PARAM_INT);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
		return $request->fetch(PDO::FETCH_ASSOC);
	}

	public static function get_all_of_child(int $id_child)
	{
		$sqlrequest = 'SELECT * FROM `reward` WHERE `id_child` = :id_child';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindparam('id_child', $id_child, PDO::PARAM_INT);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
		return $request->fetchall(PDO::FETCH_ASSOC);
	}

	public static function create(string $entitled, string $description, int $image, int $id_child) : array
	{
		// Neutralize
		$entitled = trim(htmlentities($entitled));
		$description = trim(htmlentities($description));
		$image = trim(htmlentities($image));
		$id_child = trim(htmlentities($id_child));
		
		// Verify if the reward don't not exist in database
		if (self::existe($entitled, $description)) { throw new ExceptionRessourceExist(); }

		// Create the reward
		$sqlrequest = 'INSERT INTO `reward`(`entitled`, `description`, `image`, `id_child`) VALUES (:entitled, :description, :image, :id_child)';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindParam("entitled", $entitled, PDO::PARAM_STR);
		$request->bindParam("description", $description, PDO::PARAM_STR);
		$request->bindParam("image", $image, PDO::PARAM_STR);
		$request->bindParam("id_child", $id_child, PDO::PARAM_INT);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }

		Log::write_log_application("create reward");

		return self::get(Database::get_instance()->get_connection()->lastInsertId());
	}

	public static function modify(int $id_reward, string $entitled, string $description, string $image) : array
	{
		// Neutralize
		$id_reward = trim(htmlentities($id_reward));
		$entitled = trim(htmlentities($entitled));
		$description = trim(htmlentities($description));
		$image = trim(htmlentities($image));

		if (!self::exist($id_reward)) { throw new ExceptionRessourceNotFound(); }

		// Modify the reward
		$sqlrequest = 'UPDATE `reward`(`entitled`, `description`, `image`) VALUES (:entitled, :description, :image) WHERE `id_reward` = :id_reward';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindParam("entitled", $entitled, PDO::PARAM_STR);
		$request->bindParam("description", $description, PDO::PARAM_STR);
		$request->bindParam("image", $image, PDO::PARAM_STR);
		$request->bindParam("id_reward", $id_reward, PDO::PARAM_INT);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }

		Log::write_log_application("modify reward");

		return self::get($id_reward);
	}

	public static function delete(int $id_reward) : void
	{
		//Neutralize
		$id_reward = trim(htmlentities($id_reward));

		if (!self::exist($id_reward)) { throw new ExceptionRessourceNotFound(); }

		// Delete the reward
		$sqlrequest = 'DELETE FROM `reward` WHERE `id_reward` = :id_reward';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindParam("id_reward", $id_reward);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }

		Log::write_log_application("delete reward");
	}

	public static function exist(int $id_reward) : bool
	{
		$sqlrequest = 'SELECT * FROM `reward` WHERE `id_reward` = :id_reward';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindParam("id_reward", $id_reward, PDO::PARAM_INT);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
		if ($request->rowCount() > 0) { return true; } else { return false; }
	}

	public static function existe(string $entitled, string $description) : bool
	{
		$sqlrequest = 'SELECT * FROM `reward` WHERE `description` = :description';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindParam("entitled", $entitled, PDO::PARAM_STR);
		$request->bindParam("duration", $description, PDO::PARAM_INT);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
		if ($request->rowCount() > 0) { return true; } else { return false; }
	}

}
