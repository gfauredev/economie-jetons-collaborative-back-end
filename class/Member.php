<?php

class Member
{

	public static function get(int $id_member) : array
	{
		// Neutralize
		$id_member = trim(htmlentities($id_member));

		if (!self::exist($id_member)) { throw new ExceptionRessourceNotFound(); }

		$sqlrequest = 'SELECT * FROM `member` WHERE `id_member` = :id_member';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindParam("id_member", $id_member, PDO::PARAM_INT);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
		return $request->fetch(PDO::FETCH_ASSOC);
	}

	public static function get_all() : array
	{
		$sqlrequest = 'SELECT * FROM `member`';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
		return $request->fetchAll(PDO::FETCH_ASSOC);
	}

	// ! Missing implementation
	public static function get_children_coordinated(int $id_member) : array
	{
		return array();
	}

	// ! Missing implementation
	public static function get_children_followed(int $id_member) : array
	{
		return array();
	}

	public static function create(string $name, string $surname, $birthdate, string $email, string $password, bool $validity, bool $admin = false, string $address = null, string $city = null, string $zip_code = null) : array
	{
		// Neutralize
		$name = trim(htmlentities($name));
		$surname = trim(htmlentities($surname));
		$birthdate = trim(htmlentities($birthdate));
		$email = trim(htmlentities($email));
		$password = trim(htmlentities($password));
		if (!empty($address)) { $address = trim(htmlentities($address)); }
		if (!empty($city)) { $city = trim(htmlentities($city)); }
		if (!empty($zip_code)) { $zip_code = trim(htmlentities($zip_code)); }

		// Verify if the Child don't exist in database
		if (self::existe($email)) { throw new ExceptionRessourceExist(); }

		$sqlrequest = 'INSERT INTO `role`(`entitled`) VALUES(:entitled)';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$passwordhash = password_hash($password, PASSWORD_BCRYPT);
		$request->bindparam('name', $name, PDO::PARAM_STR);
		$request->bindparam('surname', $surname, PDO::PARAM_STR);
		$request->bindparam('birthdate', $birthdate);
		$request->bindparam('email', $email, PDO::PARAM_STR);
		$request->bindparam('password', $passwordhash, PDO::PARAM_STR);
		if (!empty($address)) { $request->bindparam('address', $address, PDO::PARAM_STR); }
		if (!empty($city)) { $request->bindparam('city', $city, PDO::PARAM_STR); }
		if (!empty($zip_code)) { $request->bindparam('zip_code', $zip_code, PDO::PARAM_STR); }
		$request->bindparam('validity', $validity, PDO::PARAM_BOOL);
		$request->bindparam('admin', $admin, PDO::PARAM_BOOL);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }

		Log::write_log_application("create member");

		return self::get(Database::get_instance()->get_connection()->lastInsertId());
	}

	public static function modify(int $id_member, string $name, string $surname, $birthdate, string $email, string $password, bool $validity, bool $admin, string $address = null, string $city = null, string $zip_code = null, ) : array
	{
		// Neutralize
		$id_member = trim(htmlentities($id_member));
		$name = trim(htmlentities($name));
		$surname = trim(htmlentities($surname));
		$birthdate = trim(htmlentities($birthdate));
		$email = trim(htmlentities($email));
		$password = trim(htmlentities($password));
		if (!empty($address)) { $address = trim(htmlentities($address)); }
		if (!empty($city)) { $city = trim(htmlentities($city)); }
		if (!empty($zip_code)) { $zip_code = trim(htmlentities($zip_code)); }

		if (!self::exist($id_member)) { throw new ExceptionRessourceNotFound(); }

		$sqlrequest = 'UPDATE `role` SET `entitled` = :entitled WHERE `id_child` = :id_member';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindparam('id_member', $id_member, PDO::PARAM_INT);
		$request->bindparam('entitled', $entitled, PDO::PARAM_STR);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }

		Log::write_log_application("modify member");

		return self::get($id_member);
	}

	public static function delete(int $id_member) : void
	{
		// Neutralize
		$id_member = trim(htmlentities($id_member));

		if (!self::exist($id_member)) { throw new ExceptionRessourceNotFound(); }

		$sqlrequest = 'DELETE FROM `member` WHERE `id_member` = :id_member';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindparam('id_member', $id_member, PDO::PARAM_INT);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }

		Log::write_log_application("delete member");
	}

	public static function exist(int $id_member) : bool
	{
		$sqlrequest = 'SELECT * FROM `member` WHERE `id_member` = :id_member';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindParam("id_member", $id_member, PDO::PARAM_INT);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
		if ($request->rowCount() > 0) { return true; } else { return false; }
	}

	public static function existe(string $email) : bool
	{
		$sqlrequest = 'SELECT * FROM `member` WHERE `email` = :email';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindparam('email', $email, PDO::PARAM_STR);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
		if ($request->rowCount() > 0) { return true; } else { return false; }
	}

	// ! Missing implementation
	public static function is_coordinator_of(int $id_member, int $id_child) : bool
	{
		return false;
	}

	// ! Missing implementation
	public static function is_follower_of(int $id_member, int $id_child) : bool
	{
		return false;
	}

	public static function is_linked_to(int $id_member, int $id_child) : bool
	{
		$sqlrequest = 'SELECT * FROM `follow` WHERE `id_member` = :id_member AND `id_child` = :id_child';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindparam('id_member', $id_member, PDO::PARAM_INT);
		$request->bindparam('id_child', $id_child, PDO::PARAM_INT);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
		if ($request->rowCount() > 0) { return true; } else { return false; }
	}

}
