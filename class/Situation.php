<?php

class Situation
{
	public static function get(int $id_objective, int $id_situation) : array
	{
		// Neutralize
		$id_objective = trim(htmlentities($id_objective));
		$id_situation = trim(htmlentities($id_situation));

		if (!Objective::exist($id_objective)) { throw new ExceptionRessourceNotFound(); }
		if (!self::exist($id_objective, $id_situation)) { throw new ExceptionRessourceNotFound(); }

		$total_token = self::get_nbr_max($id_objective);
		$token_put = self::get_nbr_put($id_objective, $id_situation);
		$time = self::get_time_before_end($id_objective, $id_situation);
		return array("id_objective" => $id_objective, "id_situation" => $id_situation, "nbr_total_token" => $total_token, "nbr_token_put" => $token_put, "time" => $time);
	}

	public static function get_all(int $id_objective): array
	{
		// Neutralize
		$id_objective = trim(htmlentities($id_objective));

		if (!Objective::exist($id_objective)) { throw new ExceptionRessourceNotFound(); }

		$sqlrequest = 'SELECT DISTINCT id_situation FROM `put_token` WHERE `id_objective` = :id_objective';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindparam('id_objective', $id_objective, PDO::PARAM_INT);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
		$situations = $request->fetchAll();

		$result = array();
		foreach ($situations as $situation) {
			$res = self::get($id_objective, $situation);
			array_push($result, $res);
		}
		
		return $result;
	}

	public static function create(int $id_objective) : array
	{
		$id_objective = trim(htmlentities($id_objective));

		if (!Objective::exist($id_objective)) { throw new ExceptionRessourceNotFound(); }

		$new_id = self::get_last($id_objective) + 1;

		self::add_token($id_objective, $new_id, 1);
		self::remove_token($id_objective, $new_id, 1);

		Log::write_log_application("create situation");

		return self::get($id_objective, self::get_last($id_objective));
	}

	public static function add_token(int $id_objective, int $id_situation, int $id_member)
	{
		// Neutalize
		$id_objective = trim(htmlentities($id_objective));
		$id_situation = trim(htmlentities($id_situation));
		$id_member = trim(htmlentities($id_member));

		if (!Objective::exist($id_objective)) { throw new ExceptionRessourceNotFound(); }
		if (!self::exist($id_objective, $id_situation)) { throw new ExceptionRessourceNotFound(); }
		if (self::get_nbr_put($id_objective, $id_situation) > 0) { throw new ExceptionAddToken(); }

		$sqlrequest = 'INSERT INTO `put_token`(`id_objective`, `timestamp`, `id_situation`, `add`, `id_member`) VALUES(:id_objective, :timestamp, :id_situation, 1, :id_member)';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$date = date("Y-m-d H:i:s");
		$request->bindParam("id_objective", $id_objective);
		$request->bindParam("timestamp", $date);
		$request->bindParam("id_situation", $id_situation);
		$request->bindParam("id_member", $id_member);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }

		Log::write_log_application("add token");
	}

	public static function remove_token(int $id_objective, int $id_situation, int $id_member)
	{
		// Neutralize
		$id_objective = trim(htmlentities($id_objective));
		$id_situation = trim(htmlentities($id_situation));
		$id_member = trim(htmlentities($id_member));

		if (!Objective::exist($id_objective)) { throw new ExceptionRessourceNotFound(); }
		if (!self::exist($id_situation, $id_objective)) { throw new ExceptionRessourceNotFound(); }
		if (self::get_nbr_put($id_objective, $id_situation) > 0) { throw new ExceptionDelToken(); }

		$sqlrequest = 'INSERT INTO `put_token`(`id_objective`, `timestamp`, `id_situation`, `add`, `id_member`) VALUES(:id_objective, :timestamp, :id_situation, 0, :id_member)';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$date = date("Y-m-d H:i:s");
		$request->bindParam("id_objective", $id_objective);
		$request->bindParam("timestamp", $date);
		$request->bindParam("id_situation", $id_situation);
		$request->bindParam("id_member", $id_member);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }

		Log::write_log_application("remove token");
	}

	private static function get_last(int $id_objective): int
	{
		// Neutralize
		$id_objective = trim(htmlentities($id_objective));

		if (!Objective::exist($id_objective)) { throw new ExceptionRessourceNotFound(); }

		$sqlrequest = 'SELECT MAX(`id_situation`) FROM `put_token` WHERE `id_objective` = :id_objective';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindParam("id_objective", $id_objective);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
		return $request->fetchColumn();
	}

	public static function exist(int $id_objective, int $id_situation) : bool
	{
		$sqlrequest = 'SELECT * FROM `put_token` WHERE `id_objective` = :id_reward AND `id_situation` = :id_situation';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindParam("id_objective", $id_objective, PDO::PARAM_INT);
		$request->bindParam("id_situation", $id_situation, PDO::PARAM_INT);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
		if ($request->rowCount() > 0) { return true; } else { return false; }
	}

	private static function how_many_add(int $id_objective, int $id_situation) : int
	{
		// Neutralize
		$id_objective = trim(htmlentities($id_objective));
		$id_situation = trim(htmlentities($id_situation));

		if (!Objective::exist($id_objective)) { throw new ExceptionRessourceNotFound(); }
		if (!self::exist($id_situation, $id_objective)) { throw new ExceptionRessourceNotFound(); }

		$sqlrequest = 'SELECT COUNT(*) FROM `put_token` WHERE `id_objective` = :id_objective AND `id_situation` = :id_situation AND `add` = 1';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindParam("id_objective", $id_objective);
		$request->bindParam("id_situation", $id_situation);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
		return $request->fetchcolumn();
	}

	private static function how_many_del(int $id_objective, int $id_situation) : int
	{
		// Neutralize
		$id_objective = trim(htmlentities($id_objective));
		$id_situation = trim(htmlentities($id_situation));

		if (!Objective::exist($id_objective)) { throw new ExceptionRessourceNotFound(); }
		if (!self::exist($id_situation, $id_objective)) { throw new ExceptionRessourceNotFound(); }

		$sqlrequest = 'SELECT COUNT(*) FROM `put_token` WHERE `id_objective` = :id_objective AND `id_situation` = :id_situation AND `add` = 0';
		$request = Database::get_instance()->get_connection()->prepare($sqlrequest);
		$request->bindParam("id_objective", $id_objective);
		$request->bindParam("id_situation", $id_situation);
		if ($request->execute() === false) { throw new ExceptionDatabase(); }
		return $request->fetchcolumn();
	}

	private static function get_nbr_max(int $id_objective): int
	{
		// Neutralize
		$id_objective = trim(htmlentities($id_objective));

		if (!Objective::exist($id_objective)) { throw new ExceptionRessourceNotFound(); }

		$objective = Objective::get($id_objective);
		return $objective['nbr_token'];
	}

	private static function get_nbr_put(int $id_objective, int $id_situation): int
	{
		// Neutralize
		$id_objective = trim(htmlentities($id_objective));
		$id_situation = trim(htmlentities($id_situation));

		if (!Objective::exist($id_objective)) { throw new ExceptionRessourceNotFound(); }
		if (!self::exist($id_objective, $id_situation)) { throw new ExceptionRessourceNotFound(); }

		$add = self::how_many_add($id_objective, $id_situation);
		$del = self::how_many_del($id_objective, $id_situation);
		return ($add - $del);
	}

	private static function get_time_before_end(int $id_objective, int $id_situation): int
	{
		// Neutralize
		$id_objective = trim(htmlentities($id_objective));
		$id_situation = trim(htmlentities($id_situation));

		if (!Objective::exist($id_objective)) { throw new ExceptionRessourceNotFound(); }
		if (!self::exist($id_objective, $id_situation)) { throw new ExceptionRessourceNotFound(); }
		
		return 50;
	}
}
